# Base C Project

This is a starting point for GCC based C/C++ programs with a suite of build options

## Expected Code Structure

The build system assumes a semi-flat source code organization. Code is expected to be organized into logical units following DOD-STD-2167A sections 4.2.5 and 4.2.9, namely utilizing the concept of a Computer Software Component (CSC) as a basis of encapsulation and categorization of logic. The source and test directories are expected to be filled with CSC directories, the contents of which are a flat source file layout with both header files and source files intermixed. 

## Header Inclusion

Due to the expected code organization, all CSC folders are added into the compilation command allowing for the `#include <>` format to be used for including header files

