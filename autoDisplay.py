import pygame
import sys
from pygame.locals import *
import math

white = (255,255,255)
black = (0,0,0)

global fontSize
fontSize = 20
global cellWidth
cellWidth = fontSize * 15
global cellHeight
cellHeight = fontSize * 1.5


class dataBox():
    def __init__(self,surface):
        global fontSize
        self.pos = [0,0]
        self.screen = surface
        self.name = "No Name"
        self.value = "No Data"
        self.font = pygame.font.SysFont('couriernew',fontSize)
        
    def draw(self):
        global cellWidth
        global cellHeight
        nameXPos = self.pos[0] * cellWidth
        dataXPos = nameXPos + (cellWidth /2)
        YPos = self.pos[1] * cellHeight
        # print("Drawing at position %d %d" % (nameXPos, YPos))
        if "\0" in self.name:
            print("Null character found in ")
            print(self.name)
            self.name = self.name.replace('\0','')
        
        self.screen.blit(self.font.render(self.name,True,(0,255,0)),(nameXPos,YPos))
        self.screen.blit(self.font.render(self.value,True,(0,255,0)),(dataXPos,YPos))
        
    def update(self,value):
        if (type(value) == type(0.1)):
            self.value = "%.3f" % value
        elif (type(value) == type(1)):
            self.value = "%d" % value
        elif (type(value) == type("")):
            self.value = value
        elif value == None:
            self.value = "INV TYPE"
        else:
            self.value = str(value)


        if "\0" in self.value:
            self.value = self.value.replace("\0",'')
        # print("Set value to %s" % self.value)
        # self.draw()

class Pane(object):
    def __init__(self,windowName):
        pygame.init()
        self.windowHeight = 400
        self.windowWidth = 600
        self.font = pygame.font.SysFont('Arial', 25)
        pygame.display.set_caption(windowName)
        self.screen = pygame.display.set_mode((self.windowWidth,self.windowHeight), 0, 32)
        self.screen.fill(((0,0,0)))
        pygame.display.update()
        self.dataPoints = []
        pygame.display.update()
        self.newDataSet = False
        
    def addDataSet(self,name):
        global cellWidth
        global cellHeight
        self.newDataSet = True
        dp = dataBox(self.screen)
        if(len(name) > 11):
            name = name[:8] + "..."
        dp.name = name
        count = len(self.dataPoints)
        dp.pos[1] = count % int(math.floor(self.windowHeight / cellHeight))
        dp.pos[0] = int(math.floor(count / int(math.floor(self.windowHeight / cellHeight))))
        self.dataPoints.append(dp)

    def reset(self):
        self.dataPoints = []
        self.newDataSet = False
        
    def display(self):
        if(self.newDataSet):
            maxCord = self.dataPoints[-1].pos
            self.windowHeight = int((int(math.floor(self.windowHeight / cellHeight)) +1) * cellHeight)
            self.windowWidth = int(math.ceil((maxCord[0]+1) * cellWidth))
            self.screen = pygame.display.set_mode((self.windowWidth,self.windowHeight), 0, 32)
            self.newDataSet = False
        self.screen.fill(((0,0,0)))
        for i in range(0,len(self.dataPoints)):
            self.dataPoints[i].draw()
        pygame.display.update()
        pygame.display.flip()

    def update_point(self, num, data):

        if(num < len(self.dataPoints)):
            self.dataPoints[num].update(data)
        
        
if __name__ == "__main__":
    pane = Pane()
    for i in range(0,30):
        pane.addDataSet(str(i))
    for i in range(0,30):
        pane.dataPoints[i].draw()
    while True:
        pygame.display.flip()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit(); sys.exit();
