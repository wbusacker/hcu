import autoDisplay
import pygame
import sys
import time
import serial
import re
import struct

KNOWN_TELEMTRY_IDS = {
    1   : "CPU Utilization",
    129 : "Temp 0",
    130 : "Temp 1",
    161 : "ADC 0 Raw",
    162 : "ADC 1 Raw",
    163 : "ADC 0 Filtered",
    164 : "ADC 1 Filtered",
}


portID = 0
validPort = True
while(validPort):
    if portID > 20:
        print("No available COM devices available (Port must be under 20)")
        sys.exit(-1)
    try:
        p = "com" + str(portID)
        # print("Trying port %s" % p)
        ser = serial.Serial(port=p,baudrate=500000)
        ser.close()
        validPort = False
    except:
        portID += 1

if(len(sys.argv)== 2):
    portID = sys.argv[-1]

comPort = "com" + str(portID)

print("Opening port %s" % comPort)

ser = serial.Serial(port=comPort,baudrate=500000, timeout= 1.0)

screen = autoDisplay.Pane("HCU Auto Display")

format_codes = []

telemetry_line_re = re.compile(r"^([0-9A-X]*)\n")

format_code_lengths = {
    "c" : 1,
    "b" : 1,
    "B" : 1,
    "?" : 1,
    "h" : 2,
    "H" : 2,
    "i" : 4,
    "I" : 4,
    "l" : 4,
    "L" : 4,
    "q" : 8,
    "Q" : 8,
    "f" : 4,
    "d" : 8,
}

screen.addDataSet("Packet Count")

packet_count = 0

while(True):
    
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit(); sys.exit();
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_BACKSLASH:
                pygame.display.iconify()

    # Read in the line and figure out if we're reconfiguring or not
    line = ""
    try:
        line = ser.readline().decode("utf-8")
    except:
        pass


    tlm_line_match = telemetry_line_re.match(line)

    if "TELEMETRY DEFINITION" in line:
        # We're about to get a new telemetry definition so reload the screen
        definition = ser.readline().decode("utf-8").split(",")
        format_codes = []
        format_names = []

        screen.reset()

        for i in range(0,len(definition) // 2):
            tlm_id = int(definition[i * 2])
            name = "UNKNOWN ID {:3d}".format(tlm_id)

            if tlm_id in KNOWN_TELEMTRY_IDS:
                name = KNOWN_TELEMTRY_IDS[tlm_id]

            screen.addDataSet(name)

            format_codes.append(definition[(i * 2) + 1])

        print("New Format Definition: ", format_codes)

        screen.addDataSet("Packet Count")
        packet_count = 0

    elif tlm_line_match is not None:

        raw_data = tlm_line_match.group(1)
        
        read_index = 0
        tlm_index = 0
        while((read_index < len(raw_data)) and (tlm_index < len(format_codes))):

            format_code = format_codes[tlm_index]

            to_collect_count = format_code_lengths[format_code] * 2

            b = bytes.fromhex(raw_data[read_index:read_index + to_collect_count])

            data = struct.unpack(format_code, b)[0]

            screen.update_point(tlm_index, data)

            read_index += to_collect_count
            tlm_index += 1
        
        packet_count += 1
    
    else:
        print("Unknown Line: {:s}".format(line), end="")

    screen.dataPoints[-1].update(packet_count)
    screen.display()










