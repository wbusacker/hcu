/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  tcars
Filename: common.h
Purpose:  External CSC Header
*/

#ifndef COMMON_H
#define COMMON_H
#include <common_const.h>
#include <common_types.h>

enum Common_status_code init_common(void);
enum Common_status_code teardown_common(void);

const char* common_get_status_code_string(enum Common_status_code code);

#endif
