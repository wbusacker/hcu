/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  tcars
Filename: common_data.h
Purpose:  CSC data declaration
*/

#ifndef COMMON_DATA_H
#define COMMON_DATA_H
#include <common_const.h>
#include <common_types.h>

extern const char* common_status_code_strings[COMMON_STATUS_MAX_CODES + 1];

#endif
