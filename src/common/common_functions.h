/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  tcars
Filename: common_functions.h
Purpose:  CSC local function declarations
*/

#ifndef COMMON_FUNCTIONS_H
#define COMMON_FUNCTIONS_H
#include <common.h>
#include <common_data.h>

#endif
