/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  tcars
Filename: common_template.c
Purpose:  Example Function File
*/

#include <common_functions.h>

const char* common_get_status_code_string(enum Common_status_code code) {

    enum Common_status_code lookup = code;
    if (code > COMMON_STATUS_MAX_CODES) {
        lookup = COMMON_STATUS_MAX_CODES;
    }

    return common_status_code_strings[lookup];
}