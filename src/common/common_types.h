/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  tcars
Filename: common_types.h
Purpose:  CSC data types
*/

#ifndef COMMON_TYPES_H
#define COMMON_TYPES_H
#include <common_const.h>
#include <stdbool.h>
#include <stdint.h>

#endif
