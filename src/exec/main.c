/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: main.c
Purpose:  Program entry point
*/

#include <common.h>
#include <io.h>
#include <rate_monotonic.h>
#include <spi.h>
#include <status.h>
#include <stddef.h>
#include <tasking.h>
#include <telemetry.h>
#include <uart.h>
#include <uptime.h>

struct CSC_interface {
    enum Common_status_code (*init)(void);
    enum Common_status_code (*teardown)(void);
    bool initialized;
};

// clang-format off

/* CSC initialization order is ordered such that the first listed CSCs are initialized first

Groups are formed such that any CSCs grouped together don't interdepend on eachother but 
they depend on something in the next highest group

*/
struct CSC_interface CSCS[] = {

    /* No Dependancies */
    {.init = init_uptime,         .teardown = teardown_uptime        }, /* Need to start the timers asap*/
    {.init = init_telemetry,      .teardown = teardown_telemetry     }, /* */

    /* Init Group 2 */
    {.init = init_io,             .teardown = teardown_io            },
    {.init = init_rate_monotonic, .teardown = teardown_rate_monotonic},

    /* Init Group 3 */
    {.init = init_uart,           .teardown = teardown_uart          },
    {.init = init_spi,            .teardown = teardown_spi           },

    /* Init Group 4 */
    {.init = init_tasking,        .teardown = teardown_tasking       },
    {.init = init_status,         .teardown = teardown_status        },
};
uint64_t CSC_COUNT = sizeof(CSCS) / sizeof(CSCS[0]);

struct Tasking_entry TASKS[] = {
    {.function = cycle_status,    .arguments = NULL, .frequency = TASKING_FREQUENCY_50_HZ},
    {.function = cycle_telemetry, .arguments = NULL, .frequency = TASKING_FREQUENCY_10_HZ}
};
uint64_t TASK_COUNT = sizeof(TASKS) / sizeof(TASKS[0]);

// clang-format on

int main(void) {

    /* First mark all CSCs as uninitialized */
    for (uint64_t csc = 0; csc < CSC_COUNT; csc++) {
        CSCS[csc].initialized = false;
    }

    /* Attempt initialization of all CSCs */
    bool all_init = true;
    for (uint64_t csc = 0; csc < CSC_COUNT; csc++) {
        CSCS[csc].initialized          = true;
        enum Common_status_code status = CSCS[csc].init();

        if (status != COMMON_STATUS_OK) {
            all_init = false;
            break;
        }
    }

    if (all_init) {
        uart_printf("CSC Initialization Success\n");

        /* We need to specially announce telemetry after everything is initialized */
        telemetry_announce_definition();

        enum Common_status_code status = tasking_cycle(TASKS, TASK_COUNT);

        uart_printf("Tasking returned with code %s\n", common_get_status_code_string(status));

    } else {
        uart_printf("Not all CSCs initialized\n");
    }

    /* Teardown all CSCS in reverse order */
    for (int64_t csc = CSC_COUNT; csc >= 0; csc--) {

        if (CSCS[csc].initialized) {
            enum Common_status_code status = CSCS[csc].teardown();

            if (status != COMMON_STATUS_OK) {
                all_init = false;
                break;
            }
        }
    }

    return (0);
}