/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: fir.c
Purpose:  CSC data and initialization definitions
*/

#include <fir.h>
#include <fir_functions.h>

enum Common_status_code init_fir(void) {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    return return_code;
}

enum Common_status_code teardown_fir(void) {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    return return_code;
}
