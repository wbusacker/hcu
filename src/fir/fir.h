/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: fir.h
Purpose:  External CSC Header
*/

#ifndef FIR_H
#define FIR_H
#include <common.h>
#include <fir_const.h>
#include <fir_types.h>

enum Common_status_code init_fir(void);
enum Common_status_code teardown_fir(void);

enum Common_status_code cycle_fir(uint64_t cycle_count, void* arg);

void   fir_build_filter(struct FIR_filter* filter, double coefficients[FIR_MAX_TERMS], uint8_t used_terms);
double fir_run_filter(struct FIR_filter* filter, double new_input);

void    fir_build_fast_filter(struct FIR_fast_filter* filter, double coefficients[FIR_MAX_TERMS], uint8_t used_terms);
int16_t fir_run_fast_filter(struct FIR_fast_filter* filter, int16_t new_input);

#endif
