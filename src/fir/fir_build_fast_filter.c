/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: fir_template.c
Purpose:  Example Function File
*/

#include <fir_functions.h>
#include <uart.h>

void fir_build_fast_filter(struct FIR_fast_filter* filter, double coefficients[FIR_MAX_TERMS], uint8_t used_terms) {
    filter->used_terms       = used_terms;
    filter->natural_gain     = 0;
    filter->last_input_index = 0;

    for (uint8_t i = 0; i < filter->used_terms; i++) {

        double integer_coefficient = coefficients[i] * FIR_FAST_SCALER;

        filter->coefficients[i] = (int64_t)(integer_coefficient);
        filter->inputs[i]       = 0;
        filter->natural_gain += filter->coefficients[i];

        // uart_printf("Took Coef %21.18f -> %lld\n", coefficients[i], filter->coefficients[i]);
    }

    // uart_printf("Filter Gain %lld\n", filter->natural_gain);
}