/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: fir_template.c
Purpose:  Example Function File
*/

#include <fir_functions.h>

void fir_build_filter(struct FIR_filter* filter, double coefficients[FIR_MAX_TERMS], uint8_t used_terms) {
    filter->used_terms   = used_terms;
    filter->natural_gain = 0;

    for (uint8_t i = 0; i < filter->used_terms; i++) {
        filter->coefficients[i] = coefficients[i];
        filter->inputs[i]       = 0;
        filter->natural_gain += coefficients[i];
    }
}