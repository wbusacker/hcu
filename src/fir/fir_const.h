/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: fir_const.h
Purpose:  CSC constants
*/

#ifndef FIR_CONST_H
#define FIR_CONST_H

#define FIR_MAX_TERMS (64)

#define FIR_FAST_SCALER (0x000000FFFFFFFFFFll)

#endif
