/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: fir_data.h
Purpose:  CSC data declaration
*/

#ifndef FIR_DATA_H
#define FIR_DATA_H
#include <fir_const.h>
#include <fir_types.h>

#endif
