/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: fir_functions.h
Purpose:  CSC local function declarations
*/

#ifndef FIR_FUNCTIONS_H
#define FIR_FUNCTIONS_H
#include <fir.h>
#include <fir_data.h>

#endif
