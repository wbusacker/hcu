/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: fir_template.c
Purpose:  Example Function File
*/

#include <fir_functions.h>

int16_t fir_run_fast_filter(struct FIR_fast_filter* filter, int16_t new_input) {

    filter->inputs[filter->last_input_index] = new_input;

    uint64_t response = 0;
    for (uint8_t i = 0; i < filter->used_terms; i++) {
        response += filter->coefficients[i] * filter->inputs[(i + filter->last_input_index) % filter->used_terms];
    }

    filter->last_input_index++;
    filter->last_input_index %= filter->used_terms;

    return (int16_t)(response / filter->natural_gain);
}