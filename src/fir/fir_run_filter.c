/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: fir_template.c
Purpose:  Example Function File
*/

#include <fir_functions.h>

double fir_run_filter(struct FIR_filter* filter, double new_input) {

    /* Shift back all of the coefficients */

    for (int8_t i = filter->used_terms - 1; i > 0; i--) {
        filter->inputs[i] = filter->inputs[i - 1];
    }
    filter->inputs[0] = new_input;

    double response = 0;
    for (uint8_t i = 0; i < filter->used_terms; i++) {
        response += filter->coefficients[i] * filter->inputs[i];
    }

    return response / filter->natural_gain;
}