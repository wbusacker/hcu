/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: fir_types.h
Purpose:  CSC data types
*/

#ifndef FIR_TYPES_H
#define FIR_TYPES_H
#include <fir_const.h>
#include <stdint.h>

struct FIR_filter {
    double  coefficients[FIR_MAX_TERMS];
    double  inputs[FIR_MAX_TERMS];
    double  natural_gain;
    uint8_t used_terms;
};

struct FIR_fast_filter {
    int64_t coefficients[FIR_MAX_TERMS];
    int16_t inputs[FIR_MAX_TERMS];
    uint8_t last_input_index;
    int64_t natural_gain;
    uint8_t used_terms;
};

#endif
