/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  hcu
Filename: io.c
Purpose:  CSC data and initialization definitions
*/

#include <io.h>
#include <io_data.h>
#include <io_functions.h>

enum Common_status_code init_io(void) {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    PADCONbits.IOCON  = 0;
    PADCONbits.PMPTTL = 0;

    ANSELA = 0;
    ANSELB = 0;

    uint16_t i = 0;
    for (i = 0; i < IO_NUM_PINS; i++) {
        io_reset_pin_settings(i);
    }

    return return_code;
}

enum Common_status_code teardown_io(void) {
    enum Common_status_code return_code = COMMON_STATUS_OK;
    return return_code;
}
