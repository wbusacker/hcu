/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  hcu
Filename: io.h
Purpose:  External CSC Header
*/

#ifndef IO_H
#define IO_H
#include <common.h>
#include <io_allocation.h>
#include <io_const.h>
#include <io_types.h>
#include <stdint.h>

enum Common_status_code init_io(void);
enum Common_status_code teardown_io(void);

void              io_set(uint8_t io_number, enum IO_state_enm state);
enum IO_state_enm io_read(uint8_t io_number);
void              io_toggle(uint8_t io_number);
void io_configure_discrete(uint8_t io_number, enum IO_pin_mode_enum pin_mode, enum IO_active_enm active_state);
void io_configure_peripheral(uint8_t io_number, enum IO_peripheral peripheral);

#endif
