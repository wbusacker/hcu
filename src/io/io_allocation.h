/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  hcu
Filename: io_allocations.h
Purpose:  All names for the IO pins go here
*/

enum IO_PIN_BASE_IDS {
    IO_PIN_1  = 1, /* MCLR */
    IO_PIN_2  = 2,
    IO_PIN_3  = 3,
    IO_PIN_4  = 4, /* PGMD */
    IO_PIN_5  = 5, /* PGMC */
    IO_PIN_6  = 6,
    IO_PIN_7  = 7,
    IO_PIN_8  = 8,  /* VSS */
    IO_PIN_9  = 9,  /* OSC1 */
    IO_PIN_10 = 10, /* OSC2 */
    IO_PIN_11 = 11,
    IO_PIN_12 = 12,
    IO_PIN_13 = 13, /* VDD */
    IO_PIN_14 = 14,
    IO_PIN_15 = 15,
    IO_PIN_16 = 16,
    IO_PIN_17 = 17,
    IO_PIN_18 = 18,
    IO_PIN_19 = 19, /* VSS */
    IO_PIN_20 = 20, /* VCAP */
    IO_PIN_21 = 21,
    IO_PIN_22 = 22,
    IO_PIN_23 = 23,
    IO_PIN_24 = 24,
    IO_PIN_25 = 25,
    IO_PIN_26 = 26,
    IO_PIN_27 = 27, /* VSS */
    IO_PIN_28 = 28, /* VDD */
};

#define IO_PIN_STATUS_LED (IO_PIN_14)
#define IO_PIN_UART_RX    (IO_PIN_26)
#define IO_PIN_UART_TX    (IO_PIN_25)

#define IO_PIN_SPI_DATA_OUT (IO_PIN_23)
#define IO_PIN_SPI_DATA_IN  (IO_PIN_24)
#define IO_PIN_SPI_CLOCK    (IO_PIN_2)

#define IO_PIN_MCP3301_0 (IO_PIN_15)
#define IO_PIN_MCP3301_1 (IO_PIN_16)
