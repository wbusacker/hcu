/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  hcu
Filename: io_configure.c
Purpose:  CSC data declaration
*/

#include <io_functions.h>
#include <uart.h>

void io_configure_discrete(uint8_t io_number, enum IO_pin_mode_enum pin_mode, enum IO_active_enm active_state) {

    // io_reset_pin_settings(io_number);

    if ((io_number < IO_NUM_PINS) && ((pin_mode == IO_OUT) || (pin_mode == IO_IN))) {

        io_states[io_number].pin_mode    = pin_mode;
        io_states[io_number].active_mode = active_state;
        uint16_t bit                     = 1 << io_platform_definition[io_number].discrete_register_bit;

        switch (io_platform_definition[io_number].discrete_register_port) {
            case IO_PIC_A:
                TRISA = pin_mode == IO_IN ? (TRISA | bit) : (TRISA & ~bit);
                break;
            case IO_PIC_B:
                TRISB = pin_mode == IO_IN ? (TRISB | bit) : (TRISB & ~bit);
                break;
                break;
            default:
                break;
        }

        if (io_states[io_number].pin_mode == IO_OUT) {
            io_set(io_number, IO_CLEAR);
        }

        uart_printf("Pin %2d Set as Direction %d Mode %d\n", io_number, pin_mode, active_state);
    } else {
        uart_printf("Unable to Configure Discrete Pin %2d to Direction %d Mode %d\n",
                    io_number,
                    pin_mode,
                    active_state);
    }
}