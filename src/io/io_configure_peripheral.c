/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  hcu
Filename: io_configure.c
Purpose:  CSC data declaration
*/

#include <io_functions.h>
#include <uart.h>

void io_configure_peripheral(uint8_t io_number, enum IO_peripheral peripheral) {

    // io_reset_pin_settings(io_number);

    if (io_number < IO_NUM_PINS) {

        /* Ensure this pin can support a peripheral */
        if (io_platform_definition[io_number].peripheral_port_id != IO_NO_PERIPHERAL) {
            io_states[io_number].pin_mode = IO_PERIPHERAL;

            switch (peripheral) {
                case IO_PERIPHERAL_COMP_1_OUT:
                case IO_PERIPHERAL_COMP_2_OUT:
                case IO_PERIPHERAL_UART_1_TX:
                case IO_PERIPHERAL_UART_1_RTS:
                case IO_PERIPHERAL_UART_2_TX:
                case IO_PERIPHERAL_UART_2_RTS:
                case IO_PERIPHERAL_SPI_1_DATA_OUT:
                case IO_PERIPHERAL_SPI_1_CLOCK:
                case IO_PERIPHERAL_SPI_1_CHIP_SELECT_OUT:
                case IO_PERIPHERAL_SPI_2_DATA_OUT:
                case IO_PERIPHERAL_SPI_2_CLOCK:
                case IO_PERIPHERAL_SPI_2_CHIP_SELECT_OUT:
                case IO_PERIPHERAL_OUTPUT_COMPARE_1:
                case IO_PERIPHERAL_OUTPUT_COMPARE_2:
                case IO_PERIPHERAL_OUTPUT_COMPARE_3:
                case IO_PERIPHERAL_OUTPUT_COMPARE_2A:
                case IO_PERIPHERAL_OUTPUT_COMPARE_2B:
                case IO_PERIPHERAL_OUTPUT_COMPARE_3A:
                case IO_PERIPHERAL_OUTPUT_COMPARE_3B:
                case IO_PERIPHERAL_OUTPUT_COMPARE_4A:
                case IO_PERIPHERAL_OUTPUT_COMPARE_4B:
                case IO_PERIPHERAL_RESERVED_1:
                case IO_PERIPHERAL_SPI_3_DATA_OUT:
                case IO_PERIPHERAL_SPI_3_CLOCK:
                case IO_PERIPHERAL_SPI_3_CHIP_SELECT_OUT:
                case IO_PERIPHERAL_COMP_3_OUT:
                case IO_PERIPHERAL_RTCC_POWER_CONTROL:
                case IO_PERIPHERAL_REF_CLOCK_OUT:
                case IO_PERIPHERAL_REF_CLC_1:
                case IO_PERIPHERAL_REF_CLC_2:
                case IO_PERIPHERAL_RTCC_CLOCK_OUT: {
                    switch (io_platform_definition[io_number].peripheral_port_id) {
                        case 0:
                            _RP0R = peripheral;
                            break;
                        case 1:
                            _RP1R = peripheral;
                            break;
                        case 2:
                            _RP2R = peripheral;
                            break;
                        case 3:
                            _RP3R = peripheral;
                            break;
                        case 4:
                            _RP4R = peripheral;
                            break;
                        case 5:
                            _RP5R = peripheral;
                            break;
                        case 6:
                            _RP6R = peripheral;
                            break;
                        case 7:
                            _RP7R = peripheral;
                            break;
                        case 8:
                            _RP8R = peripheral;
                            break;
                        case 9:
                            _RP9R = peripheral;
                            break;
                        case 10:
                            _RP10R = peripheral;
                            break;
                        case 11:
                            _RP11R = peripheral;
                            break;
                        case 12:
                            _RP12R = peripheral;
                            break;
                        case 13:
                            _RP13R = peripheral;
                            break;
                        case 14:
                            _RP14R = peripheral;
                            break;
                        case 15:
                            _RP15R = peripheral;
                            break;
                        case 16:
                            _RP16R = peripheral;
                            break;
                        case 17:
                            _RP17R = peripheral;
                            break;
                        // case 18:  PIC24FJ256GA702 doesn't define this
                        //     _RP18R = peripheral;
                        //     break;
                        // case 19:  PIC24FJ256GA702 doesn't define this
                        //     _RP19R = peripheral;
                        //     break;
                        // case 20:  PIC24FJ256GA702 doesn't define this
                        //     _RP20R = peripheral;
                        //     break;
                        // case 21:  PIC24FJ256GA702 doesn't define this
                        //     _RP21R = peripheral;
                        //     break;
                        // case 22:  PIC24FJ256GA702 doesn't define this
                        //     _RP22R = peripheral;
                        //     break;
                        // case 23:  PIC24FJ256GA702 doesn't define this
                        //     _RP23R = peripheral;
                        //     break;
                        case 24:
                            _RP24R = peripheral;
                            break;
                        case 25:
                            _RP25R = peripheral;
                            break;
                        case 26:
                            _RP26R = peripheral;
                            break;
                        case 27:
                            _RP27R = peripheral;
                            break;
                            // case 28:  PIC24FJ256GA702 doesn't define this
                            //     _RP28R = peripheral;
                            //     break;
                        default:
                            uart_printf("Unabled to assigned output pin, unknown Port Number %d\n",
                                        io_platform_definition[io_number].peripheral_port_id);
                            break;
                    }
                } break;
                case IO_PERIPHERAL_OUTPUT_COMPARE_1_TRIGGER:
                    _OCTRIG1R = io_platform_definition[io_number].peripheral_port_id;
                    break;
                case IO_PERIPHERAL_EXTERNAL_INTERRUPT_1:
                    _INT1R = io_platform_definition[io_number].peripheral_port_id;
                    break;
                case IO_PERIPHERAL_EXTERNAL_INTERRUPT_2:
                    _INT2R = io_platform_definition[io_number].peripheral_port_id;
                    break;
                case IO_PERIPHERAL_EXTERNAL_INTERRUPT_3:
                    _INT3R = io_platform_definition[io_number].peripheral_port_id;
                    break;
                case IO_PERIPHERAL_EXTERNAL_INTERRUPT_4:
                    _INT4R = io_platform_definition[io_number].peripheral_port_id;
                    break;
                case IO_PERIPHERAL_OUTPUT_COMPARE_2_TRIGGER:
                    _OCTRIG2R = io_platform_definition[io_number].peripheral_port_id;
                    break;
                case IO_PERIPHERAL_TIMER_2_EXTERNAL_CLOCK:
                    _T2CKR = io_platform_definition[io_number].peripheral_port_id;
                    break;
                case IO_PERIPHERAL_TIMER_3_EXTERNAL_CLOCK:
                    _T3CKR = io_platform_definition[io_number].peripheral_port_id;
                    break;
                case IO_PERIPHERAL_INPUT_CAPTURE_MODE_1:
                    _ICM1R = io_platform_definition[io_number].peripheral_port_id;
                    break;
                case IO_PERIPHERAL_INPUT_CAPTURE_MODE_2:
                    _ICM2R = io_platform_definition[io_number].peripheral_port_id;
                    break;
                case IO_PERIPHERAL_INPUT_CAPTURE_MODE_3:
                    _ICM3R = io_platform_definition[io_number].peripheral_port_id;
                    break;
                case IO_PERIPHERAL_INPUT_CAPTURE_MODE_4:
                    _ICM4R = io_platform_definition[io_number].peripheral_port_id;
                    break;
                case IO_PERIPHERAL_INPUT_CAPTURE_1:
                    _IC1R = io_platform_definition[io_number].peripheral_port_id;
                    break;
                case IO_PERIPHERAL_INPUT_CAPTURE_2:
                    _IC2R = io_platform_definition[io_number].peripheral_port_id;
                    break;
                case IO_PERIPHERAL_INPUT_CAPTURE_3:
                    _IC3R = io_platform_definition[io_number].peripheral_port_id;
                    break;
                case IO_PERIPHERAL_OUTPUT_COMPARE_FAULT_A:
                    _OCFAR = io_platform_definition[io_number].peripheral_port_id;
                    break;
                case IO_PERIPHERAL_OUTPUT_COMPARE_FAULT_B:
                    _OCFBR = io_platform_definition[io_number].peripheral_port_id;
                    break;
                case IO_PERIPHERAL_CCP_CLOCK_INPUT_A:
                    _TCKIAR = io_platform_definition[io_number].peripheral_port_id;
                    break;
                case IO_PERIPHERAL_CCP_CLOCK_INPUT_B:
                    _TCKIBR = io_platform_definition[io_number].peripheral_port_id;
                    break;
                case IO_PERIPHERAL_UART_1_RX:
                    _U1RXR = io_platform_definition[io_number].peripheral_port_id;
                    break;
                case IO_PERIPHERAL_UART_1_CTS:
                    _U1CTSR = io_platform_definition[io_number].peripheral_port_id;
                    break;
                case IO_PERIPHERAL_UART_2_RX:
                    _U2RXR = io_platform_definition[io_number].peripheral_port_id;
                    break;
                case IO_PERIPHERAL_UART_2_CTS:
                    _U2CTSR = io_platform_definition[io_number].peripheral_port_id;
                    break;
                case IO_PERIPHERAL_SPI_1_DATA_IN:
                    _SDI1R = io_platform_definition[io_number].peripheral_port_id;
                    uart_printf("SPI1DI set to %d\n", _SDI1R);
                    break;
                case IO_PERIPHERAL_SPI_1_CLOCK_IN:
                    _SCK1R = io_platform_definition[io_number].peripheral_port_id;
                    break;
                case IO_PERIPHERAL_SPI_1_CHIP_SELECT_IN:
                    _SS1R = io_platform_definition[io_number].peripheral_port_id;
                    break;
                case IO_PERIPHERAL_SPI_2_DATA_IN:
                    _SDI2R = io_platform_definition[io_number].peripheral_port_id;
                    break;
                case IO_PERIPHERAL_SPI_2_CLOCK_IN:
                    _SCK2R = io_platform_definition[io_number].peripheral_port_id;
                    break;
                case IO_PERIPHERAL_SPI_2_CHIP_SELECT_IN:
                    _SS2R = io_platform_definition[io_number].peripheral_port_id;
                    break;
                case IO_PERIPHERAL_GENERIC_TIMER_EXTERNAL_CLOCK:
                    _TXCKR = io_platform_definition[io_number].peripheral_port_id;
                    break;
                case IO_PERIPHERAL_CLC_IN_A:
                    _CLCINAR = io_platform_definition[io_number].peripheral_port_id;
                    break;
                case IO_PERIPHERAL_CLC_IN_B:
                    _CLCINBR = io_platform_definition[io_number].peripheral_port_id;
                    break;
                case IO_PERIPHERAL_SPI_3_DATA_IN:
                    _SDI3R = io_platform_definition[io_number].peripheral_port_id;
                    break;
                case IO_PERIPHERAL_SPI_3_CLOCK_IN:
                    _SCK3R = io_platform_definition[io_number].peripheral_port_id;
                    break;
                case IO_PERIPHERAL_SPI_3_CHIP_SELECT_IN:
                    _SS3R = io_platform_definition[io_number].peripheral_port_id;
                    break;
                default:
                    uart_printf("Failed to configure Peripheral, unknown Input ID %d\n", peripheral);
                    break;
            }

            uart_printf("Peripheral %2d set to pin %d\n", peripheral, io_number);
        } else {
            uart_printf("Peripheral %2d cannot be assigned to pin %d\n", peripheral, io_number);
        }
    }
}