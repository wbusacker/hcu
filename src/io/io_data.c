/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  hcu
Filename: io_data.h
Purpose:  CSC data declaration
*/

#ifndef IO_DATA_H
#define IO_DATA_H
#include <io_const.h>
#include <io_types.h>

// clang-format off
const struct io_pin_definition io_platform_definition[IO_NUM_PINS] = {
  /* PIN  0 */ {.discrete_register_port = IO_SOFTWARE, .discrete_register_bit =  0, .analog_id = IO_NO_ANALOG, .peripheral_port_id = IO_NO_PERIPHERAL}, 
  /* PIN  1 */ {.discrete_register_port = IO_SOFTWARE, .discrete_register_bit =  0, .analog_id = IO_NO_ANALOG, .peripheral_port_id = IO_NO_PERIPHERAL}, 
  /* PIN  2 */ {.discrete_register_port = IO_PIC_A   , .discrete_register_bit =  0, .analog_id = 0,            .peripheral_port_id = 26}, 
  /* PIN  3 */ {.discrete_register_port = IO_PIC_A   , .discrete_register_bit =  1, .analog_id = 1,            .peripheral_port_id = 27}, 
  /* PIN  4 */ {.discrete_register_port = IO_PIC_B   , .discrete_register_bit =  0, .analog_id = IO_NO_ANALOG, .peripheral_port_id = 0}, 
  /* PIN  5 */ {.discrete_register_port = IO_PIC_B   , .discrete_register_bit =  1, .analog_id = 3,            .peripheral_port_id = 1}, 
  /* PIN  6 */ {.discrete_register_port = IO_PIC_B   , .discrete_register_bit =  2, .analog_id = 4,            .peripheral_port_id = 2}, 
  /* PIN  7 */ {.discrete_register_port = IO_PIC_B   , .discrete_register_bit =  3, .analog_id = 5,            .peripheral_port_id = 3}, 
  /* PIN  8 */ {.discrete_register_port = IO_SOFTWARE, .discrete_register_bit =  0, .analog_id = IO_NO_ANALOG, .peripheral_port_id = IO_NO_PERIPHERAL}, 
  /* PIN  9 */ {.discrete_register_port = IO_PIC_A   , .discrete_register_bit =  2, .analog_id = IO_NO_ANALOG, .peripheral_port_id = IO_NO_PERIPHERAL}, 
  /* PIN 10 */ {.discrete_register_port = IO_PIC_A   , .discrete_register_bit =  3, .analog_id = IO_NO_ANALOG, .peripheral_port_id = IO_NO_PERIPHERAL}, 
  /* PIN 11 */ {.discrete_register_port = IO_PIC_B   , .discrete_register_bit =  4, .analog_id = IO_NO_ANALOG, .peripheral_port_id = 4}, 
  /* PIN 12 */ {.discrete_register_port = IO_PIC_A   , .discrete_register_bit =  4, .analog_id = IO_NO_ANALOG, .peripheral_port_id = IO_NO_PERIPHERAL}, 
  /* PIN 13 */ {.discrete_register_port = IO_SOFTWARE, .discrete_register_bit =  0, .analog_id = IO_NO_ANALOG, .peripheral_port_id = IO_NO_PERIPHERAL}, 
  /* PIN 14 */ {.discrete_register_port = IO_PIC_B   , .discrete_register_bit =  5, .analog_id = IO_NO_ANALOG, .peripheral_port_id = 5}, 
  /* PIN 15 */ {.discrete_register_port = IO_PIC_B   , .discrete_register_bit =  6, .analog_id = IO_NO_ANALOG, .peripheral_port_id = 6}, 
  /* PIN 16 */ {.discrete_register_port = IO_PIC_B   , .discrete_register_bit =  7, .analog_id = IO_NO_ANALOG, .peripheral_port_id = 7}, 
  /* PIN 17 */ {.discrete_register_port = IO_PIC_B   , .discrete_register_bit =  8, .analog_id = IO_NO_ANALOG, .peripheral_port_id = 8}, 
  /* PIN 18 */ {.discrete_register_port = IO_PIC_B   , .discrete_register_bit =  9, .analog_id = IO_NO_ANALOG, .peripheral_port_id = 9}, 
  /* PIN 19 */ {.discrete_register_port = IO_SOFTWARE, .discrete_register_bit =  0, .analog_id = IO_NO_ANALOG, .peripheral_port_id = IO_NO_PERIPHERAL}, 
  /* PIN 20 */ {.discrete_register_port = IO_SOFTWARE, .discrete_register_bit =  0, .analog_id = IO_NO_ANALOG, .peripheral_port_id = IO_NO_PERIPHERAL}, 
  /* PIN 21 */ {.discrete_register_port = IO_PIC_B   , .discrete_register_bit = 10, .analog_id = IO_NO_ANALOG, .peripheral_port_id = 10}, 
  /* PIN 22 */ {.discrete_register_port = IO_PIC_B   , .discrete_register_bit = 11, .analog_id = IO_NO_ANALOG, .peripheral_port_id = 11}, 
  /* PIN 23 */ {.discrete_register_port = IO_PIC_B   , .discrete_register_bit = 12, .analog_id = 8,            .peripheral_port_id = 12}, 
  /* PIN 24 */ {.discrete_register_port = IO_PIC_B   , .discrete_register_bit = 13, .analog_id = 7,            .peripheral_port_id = 13}, 
  /* PIN 25 */ {.discrete_register_port = IO_PIC_B   , .discrete_register_bit = 14, .analog_id = 6,            .peripheral_port_id = 14}, 
  /* PIN 26 */ {.discrete_register_port = IO_PIC_B   , .discrete_register_bit = 15, .analog_id = 9,            .peripheral_port_id = 15}, 
  /* PIN 27 */ {.discrete_register_port = IO_SOFTWARE, .discrete_register_bit =  0, .analog_id = IO_NO_ANALOG, .peripheral_port_id = IO_NO_PERIPHERAL}, 
  /* PIN 28 */ {.discrete_register_port = IO_SOFTWARE, .discrete_register_bit =  0, .analog_id = IO_NO_ANALOG, .peripheral_port_id = IO_NO_PERIPHERAL}  
};
// clang-format on

struct io_pin io_states[IO_NUM_PINS];

#endif
