/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  hcu
Filename: io_data.h
Purpose:  CSC data declaration
*/

#ifndef IO_DATA_H
#define IO_DATA_H
#include <io_const.h>
#include <io_types.h>
#include <stdbool.h>

extern const struct io_pin_definition io_platform_definition[IO_NUM_PINS];
extern struct io_pin                  io_states[IO_NUM_PINS];

extern bool io_configured;

#endif
