/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  hcu
Filename: io_functions.h
Purpose:  CSC local function declarations
*/

#ifndef IO_FUNCTIONS_H
#define IO_FUNCTIONS_H
#include <io.h>
#include <io_data.h>

void io_reset_pin_settings(uint8_t io_num);

#endif
