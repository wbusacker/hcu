/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  hcu
Filename: io_set.c
Purpose:  CSC data declaration
*/

#include <io_functions.h>

enum IO_state_enm io_read(uint8_t io_number) {

    enum IO_state_enm res = IO_CLEAR;

    /* Explicitly range check first */
    if (io_number < IO_NUM_PINS) {
        if ((io_states[io_number].pin_mode == IO_OUT) || (io_states[io_number].pin_mode == IO_IN)) {
            uint16_t port = 0;

            switch (io_platform_definition[io_number].discrete_register_port) {
                case IO_PIC_A:
                    port = PORTA;
                    break;
                case IO_PIC_B:
                    port = PORTB;
                    break;
                default:
                    break;
            }

            uint16_t bit = (port >> io_platform_definition[io_number].discrete_register_bit) & 0b1;

            if (io_states[io_number].active_mode == IO_ACTIVE_HIGH) {
                res = (bit == 0) ? IO_CLEAR : IO_SET;
            } else {
                res = (bit == 0) ? IO_SET : IO_CLEAR;
            }
        }
    }

    return res;
}