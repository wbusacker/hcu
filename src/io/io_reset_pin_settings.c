/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  hcu
Filename: io_set.c
Purpose:  CSC data declaration
*/

#include <io_functions.h>

void io_reset_pin_settings(uint8_t io_num) {

    if (io_num < IO_NUM_PINS) {

        /* See if any of the input peripherals are configured to the io pin */
        if (_OCTRIG1R == io_platform_definition[io_num].peripheral_port_id) {
            _OCTRIG1R = IO_PERIPHERAL_NOT_ASSIGNED;
        }
        if (_INT1R == io_platform_definition[io_num].peripheral_port_id) {
            _INT1R = IO_PERIPHERAL_NOT_ASSIGNED;
        }
        if (_INT2R == io_platform_definition[io_num].peripheral_port_id) {
            _INT2R = IO_PERIPHERAL_NOT_ASSIGNED;
        }
        if (_INT3R == io_platform_definition[io_num].peripheral_port_id) {
            _INT3R = IO_PERIPHERAL_NOT_ASSIGNED;
        }
        if (_INT4R == io_platform_definition[io_num].peripheral_port_id) {
            _INT4R = IO_PERIPHERAL_NOT_ASSIGNED;
        }
        if (_OCTRIG2R == io_platform_definition[io_num].peripheral_port_id) {
            _OCTRIG2R = IO_PERIPHERAL_NOT_ASSIGNED;
        }
        if (_T2CKR == io_platform_definition[io_num].peripheral_port_id) {
            _T2CKR = IO_PERIPHERAL_NOT_ASSIGNED;
        }
        if (_T3CKR == io_platform_definition[io_num].peripheral_port_id) {
            _T3CKR = IO_PERIPHERAL_NOT_ASSIGNED;
        }
        if (_ICM1R == io_platform_definition[io_num].peripheral_port_id) {
            _ICM1R = IO_PERIPHERAL_NOT_ASSIGNED;
        }
        if (_ICM2R == io_platform_definition[io_num].peripheral_port_id) {
            _ICM2R = IO_PERIPHERAL_NOT_ASSIGNED;
        }
        if (_ICM3R == io_platform_definition[io_num].peripheral_port_id) {
            _ICM3R = IO_PERIPHERAL_NOT_ASSIGNED;
        }
        if (_ICM4R == io_platform_definition[io_num].peripheral_port_id) {
            _ICM4R = IO_PERIPHERAL_NOT_ASSIGNED;
        }
        if (_IC1R == io_platform_definition[io_num].peripheral_port_id) {
            _IC1R = IO_PERIPHERAL_NOT_ASSIGNED;
        }
        if (_IC2R == io_platform_definition[io_num].peripheral_port_id) {
            _IC2R = IO_PERIPHERAL_NOT_ASSIGNED;
        }
        if (_IC3R == io_platform_definition[io_num].peripheral_port_id) {
            _IC3R = IO_PERIPHERAL_NOT_ASSIGNED;
        }
        if (_OCFAR == io_platform_definition[io_num].peripheral_port_id) {
            _OCFAR = IO_PERIPHERAL_NOT_ASSIGNED;
        }
        if (_OCFBR == io_platform_definition[io_num].peripheral_port_id) {
            _OCFBR = IO_PERIPHERAL_NOT_ASSIGNED;
        }
        if (_TCKIAR == io_platform_definition[io_num].peripheral_port_id) {
            _TCKIAR = IO_PERIPHERAL_NOT_ASSIGNED;
        }
        if (_TCKIBR == io_platform_definition[io_num].peripheral_port_id) {
            _TCKIBR = IO_PERIPHERAL_NOT_ASSIGNED;
        }
        if (_U1RXR == io_platform_definition[io_num].peripheral_port_id) {
            _U1RXR = IO_PERIPHERAL_NOT_ASSIGNED;
        }
        if (_U1CTSR == io_platform_definition[io_num].peripheral_port_id) {
            _U1CTSR = IO_PERIPHERAL_NOT_ASSIGNED;
        }
        if (_U2RXR == io_platform_definition[io_num].peripheral_port_id) {
            _U2RXR = IO_PERIPHERAL_NOT_ASSIGNED;
        }
        if (_U2CTSR == io_platform_definition[io_num].peripheral_port_id) {
            _U2CTSR = IO_PERIPHERAL_NOT_ASSIGNED;
        }
        if (_SDI1R == io_platform_definition[io_num].peripheral_port_id) {
            _SDI1R = IO_PERIPHERAL_NOT_ASSIGNED;
        }
        if (_SCK1R == io_platform_definition[io_num].peripheral_port_id) {
            _SCK1R = IO_PERIPHERAL_NOT_ASSIGNED;
        }
        if (_SS1R == io_platform_definition[io_num].peripheral_port_id) {
            _SS1R = IO_PERIPHERAL_NOT_ASSIGNED;
        }
        if (_SDI2R == io_platform_definition[io_num].peripheral_port_id) {
            _SDI2R = IO_PERIPHERAL_NOT_ASSIGNED;
        }
        if (_SCK2R == io_platform_definition[io_num].peripheral_port_id) {
            _SCK2R = IO_PERIPHERAL_NOT_ASSIGNED;
        }
        if (_SS2R == io_platform_definition[io_num].peripheral_port_id) {
            _SS2R = IO_PERIPHERAL_NOT_ASSIGNED;
        }
        if (_TXCKR == io_platform_definition[io_num].peripheral_port_id) {
            _TXCKR = IO_PERIPHERAL_NOT_ASSIGNED;
        }
        if (_CLCINAR == io_platform_definition[io_num].peripheral_port_id) {
            _CLCINAR = IO_PERIPHERAL_NOT_ASSIGNED;
        }
        if (_CLCINBR == io_platform_definition[io_num].peripheral_port_id) {
            _CLCINBR = IO_PERIPHERAL_NOT_ASSIGNED;
        }
        if (_SDI3R == io_platform_definition[io_num].peripheral_port_id) {
            _SDI3R = IO_PERIPHERAL_NOT_ASSIGNED;
        }
        if (_SCK3R == io_platform_definition[io_num].peripheral_port_id) {
            _SCK3R = IO_PERIPHERAL_NOT_ASSIGNED;
        }
        if (_SS3R == io_platform_definition[io_num].peripheral_port_id) {
            _SS3R = IO_PERIPHERAL_NOT_ASSIGNED;
        }

        /* Find the output Peripheral pin and un-assign it as well */
        switch (io_platform_definition[io_num].peripheral_port_id) {
            case 0:
                _RP0R = IO_PERIPHERAL_NOT_ASSIGNED;
                break;
            case 1:
                _RP1R = IO_PERIPHERAL_NOT_ASSIGNED;
                break;
            case 2:
                _RP2R = IO_PERIPHERAL_NOT_ASSIGNED;
                break;
            case 3:
                _RP3R = IO_PERIPHERAL_NOT_ASSIGNED;
                break;
            case 4:
                _RP4R = IO_PERIPHERAL_NOT_ASSIGNED;
                break;
            case 5:
                _RP5R = IO_PERIPHERAL_NOT_ASSIGNED;
                break;
            case 6:
                _RP6R = IO_PERIPHERAL_NOT_ASSIGNED;
                break;
            case 7:
                _RP7R = IO_PERIPHERAL_NOT_ASSIGNED;
                break;
            case 8:
                _RP8R = IO_PERIPHERAL_NOT_ASSIGNED;
                break;
            case 9:
                _RP9R = IO_PERIPHERAL_NOT_ASSIGNED;
                break;
            case 10:
                _RP10R = IO_PERIPHERAL_NOT_ASSIGNED;
                break;
            case 11:
                _RP11R = IO_PERIPHERAL_NOT_ASSIGNED;
                break;
            case 12:
                _RP12R = IO_PERIPHERAL_NOT_ASSIGNED;
                break;
            case 13:
                _RP13R = IO_PERIPHERAL_NOT_ASSIGNED;
                break;
            case 14:
                _RP14R = IO_PERIPHERAL_NOT_ASSIGNED;
                break;
            case 15:
                _RP15R = IO_PERIPHERAL_NOT_ASSIGNED;
                break;
            case 16:
                _RP16R = IO_PERIPHERAL_NOT_ASSIGNED;
                break;
            case 17:
                _RP17R = IO_PERIPHERAL_NOT_ASSIGNED;
                break;
            // case 18:  PIC24FJ256GA702 doesn't define this
            //     _RP18R = IO_PERIPHERAL_NOT_ASSIGNED;
            //     break;
            // case 19:  PIC24FJ256GA702 doesn't define this
            //     _RP19R = IO_PERIPHERAL_NOT_ASSIGNED;
            //     break;
            // case 20:  PIC24FJ256GA702 doesn't define this
            //     _RP20R = IO_PERIPHERAL_NOT_ASSIGNED;
            //     break;
            // case 21:  PIC24FJ256GA702 doesn't define this
            //     _RP21R = IO_PERIPHERAL_NOT_ASSIGNED;
            //     break;
            // case 22:  PIC24FJ256GA702 doesn't define this
            //     _RP22R = IO_PERIPHERAL_NOT_ASSIGNED;
            //     break;
            // case 23:  PIC24FJ256GA702 doesn't define this
            //     _RP23R = IO_PERIPHERAL_NOT_ASSIGNED;
            //     break;
            case 24:
                _RP24R = IO_PERIPHERAL_NOT_ASSIGNED;
                break;
            case 25:
                _RP25R = IO_PERIPHERAL_NOT_ASSIGNED;
                break;
            case 26:
                _RP26R = IO_PERIPHERAL_NOT_ASSIGNED;
                break;
            case 27:
                _RP27R = IO_PERIPHERAL_NOT_ASSIGNED;
                break;
                // case 28:  PIC24FJ256GA702 doesn't define this
                //     _RP28R = IO_PERIPHERAL_NOT_ASSIGNED;
                //     break;
        }

        /* Reset all Discrete IO registers as well */
        uint16_t bit = 1 << io_platform_definition[io_num].discrete_register_bit;
        switch (io_platform_definition[io_num].discrete_register_port) {
            case IO_PIC_A:
                TRISA |= bit;
                PORTA &= ~bit;
                LATA &= ~bit;
                break;
            case IO_PIC_B:
                TRISB |= bit;
                PORTB &= ~bit;
                LATB &= ~bit;
                break;
            default:
                break;
        }
    }
}