/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  hcu
Filename: io_set.c
Purpose:  CSC data declaration
*/

#include <io_functions.h>

void io_set(uint8_t io_number, enum IO_state_enm state) {

    /* Explicitly range check first */
    if (io_number < IO_NUM_PINS) {
        if (io_states[io_number].pin_mode == IO_OUT) {

            bool output_one = false;
            if (state == IO_SET) {
                output_one = (io_states[io_number].active_mode == IO_ACTIVE_HIGH);
            } else {
                output_one = (io_states[io_number].active_mode == IO_ACTIVE_LOW);
            }

            uint16_t bit = 1 << io_platform_definition[io_number].discrete_register_bit;

            switch (io_platform_definition[io_number].discrete_register_port) {
                case IO_PIC_A:
                    LATA = output_one ? (LATA | bit) : (LATA & ~bit);
                    break;
                case IO_PIC_B:
                    LATB = output_one ? (LATB | bit) : (LATB & ~bit);
                    break;
                default:
                    break;
            }
        }
    }
}