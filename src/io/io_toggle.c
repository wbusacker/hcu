/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  hcu
Filename: io_set.c
Purpose:  CSC data declaration
*/

#include <io_functions.h>

void io_toggle(uint8_t io_number) {

    if (io_number < IO_NUM_PINS) {
        if (io_states[io_number].pin_mode == IO_OUT) {

            uint16_t bit = 1 << io_platform_definition[io_number].discrete_register_bit;

            switch (io_platform_definition[io_number].discrete_register_port) {
                case IO_PIC_A:
                    LATA ^= bit;
                    break;
                case IO_PIC_B:
                    LATB ^= bit;
                    break;
                default:
                    break;
            }
        }
    }
}