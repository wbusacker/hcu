/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  hcu
Filename: io_types.h
Purpose:  CSC data types
*/

#ifndef IO_TYPES_H
#define IO_TYPES_H
#include <io_const.h>
#include <stdint.h>

struct io_pin_definition {
    enum IO_port_enm discrete_register_port;
    uint8_t          discrete_register_bit;
    uint8_t          peripheral_port_id;
    uint8_t          analog_id;
};

struct io_pin {
    enum IO_pin_mode_enum pin_mode;
    enum IO_active_enm    active_mode;
};

#endif
