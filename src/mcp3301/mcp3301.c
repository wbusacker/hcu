/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: mcp3301.c
Purpose:  CSC data and initialization definitions
*/

#include <mcp3301.h>
#include <mcp3301_functions.h>

enum Common_status_code init_mcp3301(void) {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    return return_code;
}

enum Common_status_code teardown_mcp3301(void) {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    return return_code;
}
