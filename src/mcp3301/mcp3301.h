/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: mcp3301.h
Purpose:  External CSC Header
*/

#ifndef MCP3301_H
#define MCP3301_H
#include <common.h>
#include <mcp3301_const.h>
#include <mcp3301_types.h>

enum Common_status_code init_mcp3301(void);
enum Common_status_code teardown_mcp3301(void);

MCP3301_device_id mcp3301_register_device(uint8_t chip_select_io_number);

int16_t mcp3301_get_raw_value(MCP3301_device_id id);
float   mcp3301_get_voltage(MCP3301_device_id id);

#endif
