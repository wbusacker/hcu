/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: mcp3301_const.h
Purpose:  CSC constants
*/

#ifndef MCP3301_CONST_H
#define MCP3301_CONST_H

#include <spi.h>

#define MCP3301_MAX_DEVICES (16)

#define MCP3301_DATA_MASK        (0x1FFF)
#define MCP3301_SIGN_MASK        (0x1000)
#define MCP3301_SIGN_EXTEND_MASK (0xF000)

#define MCP3301_NUM_DATA_BYTES (2)

#define MCP3301_REF_VOLTAGE (5.166)
#define MCP3301_V_PER_LSB   (MCP3301_REF_VOLTAGE / 4096.0)

#define MCP3301_CLOCK_SPEED_HZ (500000)
#define MCP3301_SPI_MODE       SPI_MODE_0

#endif
