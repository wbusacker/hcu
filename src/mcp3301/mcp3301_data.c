/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: mcp3301_data.c
Purpose:  CSC data definition
*/

#include <mcp3301_data.h>

struct MCP3301_device mcp3301_devices[MCP3301_MAX_DEVICES];
MCP3301_device_id     mcp3301_num_devices;