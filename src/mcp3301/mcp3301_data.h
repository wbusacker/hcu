/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: mcp3301_data.h
Purpose:  CSC data declaration
*/

#ifndef MCP3301_DATA_H
#define MCP3301_DATA_H
#include <mcp3301_const.h>
#include <mcp3301_types.h>

extern struct MCP3301_device mcp3301_devices[MCP3301_MAX_DEVICES];
extern MCP3301_device_id     mcp3301_num_devices;

#endif
