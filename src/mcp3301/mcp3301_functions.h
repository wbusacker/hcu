/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: mcp3301_functions.h
Purpose:  CSC local function declarations
*/

#ifndef MCP3301_FUNCTIONS_H
#define MCP3301_FUNCTIONS_H
#include <mcp3301.h>
#include <mcp3301_data.h>

#endif
