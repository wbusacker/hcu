/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: mcp3301_template.c
Purpose:  Example Function File
*/

#include <mcp3301_functions.h>
#include <stddef.h>

int16_t mcp3301_get_raw_value(MCP3301_device_id id) {

    int16_t adc_value = 0;

    if (id < mcp3301_num_devices) {

        uint8_t raw_bytes[MCP3301_NUM_DATA_BYTES] = {0x77, 0x77};

        spi_transaction(mcp3301_devices[id].spi_device_id, NULL, raw_bytes, MCP3301_NUM_DATA_BYTES);

        adc_value = (raw_bytes[0] << COMMON_BITS_PER_BYTE) | raw_bytes[1];

        adc_value &= MCP3301_DATA_MASK;

        if (adc_value & MCP3301_SIGN_MASK) {
            adc_value |= MCP3301_SIGN_EXTEND_MASK;
        }
    }

    return adc_value;
}