/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: mcp3301_template.c
Purpose:  Example Function File
*/

#include <mcp3301_functions.h>

float mcp3301_get_voltage(MCP3301_device_id id) {

    float voltage = 0;

    if (id < mcp3301_num_devices) {

        int16_t raw_bytes = mcp3301_get_raw_value(id);

        voltage = raw_bytes * MCP3301_V_PER_LSB;
    }

    return voltage;
}