/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: mcp3301_template.c
Purpose:  Example Function File
*/

#include <mcp3301_functions.h>

MCP3301_device_id mcp3301_register_device(uint8_t chip_select_io_number) {

    MCP3301_device_id new_device = MCP3301_MAX_DEVICES;

    if (mcp3301_num_devices < MCP3301_MAX_DEVICES) {

        new_device = mcp3301_num_devices;
        mcp3301_num_devices++;

        mcp3301_devices[new_device].spi_device_id =
          spi_register_device(chip_select_io_number, IO_ACTIVE_LOW, MCP3301_CLOCK_SPEED_HZ, MCP3301_SPI_MODE);
    }

    return new_device;
}