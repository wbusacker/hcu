/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: mcp3301_types.h
Purpose:  CSC data types
*/

#ifndef MCP3301_TYPES_H
#define MCP3301_TYPES_H
#include <mcp3301_const.h>
#include <spi.h>
#include <stdint.h>

typedef uint8_t MCP3301_device_id;

struct MCP3301_device {
    SPI_device_id spi_device_id;
};

#endif
