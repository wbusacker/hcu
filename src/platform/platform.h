/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  hcu
Filename: platform.h
Purpose:  External CSC Header
*/

#ifndef PLATFORM_H
#define PLATFORM_H

#include <p24FJ256GA702.h>
#define NUM_PINS                        (28)
#define BASE_CLOCK_FREQUENCY_HZ         (16E6l)
#define TIMER_BASE_DIVIDER              (2)
#define PERIPHERICAL_CLOCK_FREQUENCY_HZ (BASE_CLOCK_FREQUENCY_HZ / TIMER_BASE_DIVIDER)

#endif
