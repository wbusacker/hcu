/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: rate_monotonic.c
Purpose:  CSC data and initialization definitions
*/

#include <rate_monotonic.h>
#include <rate_monotonic_functions.h>

enum Common_status_code init_rate_monotonic(void) {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    return return_code;
}

enum Common_status_code teardown_rate_monotonic(void) {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    return return_code;
}
