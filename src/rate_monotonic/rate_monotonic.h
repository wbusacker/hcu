/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: rate_monotonic.h
Purpose:  External CSC Header
*/

#ifndef RATE_MONOTONIC_H
#define RATE_MONOTONIC_H
#include <common.h>
#include <rate_monotonic_const.h>
#include <rate_monotonic_types.h>

enum Common_status_code init_rate_monotonic(void);
enum Common_status_code teardown_rate_monotonic(void);

void rate_monotonic_init(struct Rate_monotonic_t* rm, uint64_t period_ns, uint64_t first_unlock_time_ns);
void rate_monotonic_suspend(struct Rate_monotonic_t* rm);
void rate_monotonic_set_period(struct Rate_monotonic_t* rm, uint64_t period_ns);

void rate_monotonic_get_statistics(struct Rate_monotonic_t* rm,
                                   int64_t*                 average_time_ns,
                                   int64_t*                 shortest_time_ns,
                                   int64_t*                 longest_time_ns,
                                   uint64_t*                missed);

enum Rate_monotonic_state_enm rate_monotonic_cycle(struct Rate_monotonic_t* rm);

#endif
