/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: rate_monotonic_const.h
Purpose:  CSC constants
*/

#ifndef RATE_MONOTONIC_CONST_H
#define RATE_MONOTONIC_CONST_H

#define RATE_MONOTONIC_SLEEP_THRESHOLD_NS (1E3)
#define RATE_MONOTONIC_IMMEDIATE_UNLOCK   (0)

enum Rate_monotonic_state_enm {
    RATE_MONOTONIC_START_DELAYED,
    RATE_MONOTONIC_START_IMMEDIATE,
    RATE_MONOTONIC_ENABLED,
    RATE_MONOTONIC_DISABLED
};

#endif
