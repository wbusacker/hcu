/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: rate_monotonic_cycle.c
Purpose:  CSC data and initialization definitions
*/

#include <rate_monotonic_functions.h>
#include <uptime.h>

enum Rate_monotonic_state_enm rate_monotonic_cycle(struct Rate_monotonic_t* rm) {

    int64_t current_time = uptime_nanoseconds();

    /* If we've never cycled before, figure out how to calculate the next unlock */
    if (rm->state == RATE_MONOTONIC_START_DELAYED) {
        rm->state = RATE_MONOTONIC_ENABLED;
    } else if (rm->state == RATE_MONOTONIC_START_IMMEDIATE) {
        rm->state = RATE_MONOTONIC_ENABLED;

        /* Just set the next unlock time to right now. When we evaluate the sleep
            time, we'll be negative and guarantee trigger a missed period so
            subtract one off
        */
        rm->next_unlock_ns      = current_time;
        rm->missed_periods      = -1;
        rm->last_unlock_time_ns = current_time;
    }

    int64_t time_remaining = rm->next_unlock_ns - current_time;
    int64_t alive_time     = current_time - rm->last_unlock_time_ns;
    rm->total_awake_ns += alive_time;

    if (alive_time > rm->longest_cycle_ns) {
        rm->longest_cycle_ns = alive_time;
    }

    if (alive_time < rm->shortest_cycle_ns) {
        rm->shortest_cycle_ns = alive_time;
    }

    /* Did we blow past the period? */
    if (time_remaining < 0) {
        rm->missed_periods++;
    } else {

        while (time_remaining > 0) {

            /* If we have more than a system tick to go, use a CPU deep sleep */
            if (time_remaining > UPTIME_NS_PER_TICK) {
                __asm__ volatile("PWRSAV #1");
            }

            current_time   = uptime_nanoseconds();
            time_remaining = rm->next_unlock_ns - current_time;
        }
    }

    rm->total_periods++;
    rm->next_unlock_ns += rm->period_ns;
    rm->last_unlock_time_ns = current_time;

    return rm->state;
}
