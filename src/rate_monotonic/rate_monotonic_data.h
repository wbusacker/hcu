/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: rate_monotonic_data.h
Purpose:  CSC data declaration
*/

#ifndef RATE_MONOTONIC_DATA_H
#define RATE_MONOTONIC_DATA_H
#include <rate_monotonic_const.h>
#include <rate_monotonic_types.h>

#endif
