/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: rate_monotonic_functions.h
Purpose:  CSC local function declarations
*/

#ifndef RATE_MONOTONIC_FUNCTIONS_H
#define RATE_MONOTONIC_FUNCTIONS_H
#include <rate_monotonic.h>
#include <rate_monotonic_data.h>

#endif
