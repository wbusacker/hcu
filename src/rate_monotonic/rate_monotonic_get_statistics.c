/*
 Copyright (c) 2023 Will Busacker
 See project license for more details

 Project:  hcu
 Filename: rate_monotonic_template.c
 Purpose:  Example Function File
 */

#include <rate_monotonic_functions.h>

void rate_monotonic_get_statistics(struct Rate_monotonic_t* rm,
                                   int64_t*                 average_time_ns,
                                   int64_t*                 shortest_time_ns,
                                   int64_t*                 longest_time_ns,
                                   uint64_t*                missed) {

    *shortest_time_ns = rm->shortest_cycle_ns;
    *longest_time_ns  = rm->longest_cycle_ns;
    *average_time_ns  = rm->total_awake_ns / rm->total_periods;
    *missed           = rm->missed_periods;

    return;
}