/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: rate_monotonic_template.c
Purpose:  Example Function File
*/

#include <rate_monotonic_functions.h>

void rate_monotonic_init(struct Rate_monotonic_t* rm, uint64_t period_ns, uint64_t first_unlock_time_ns) {

    rm->period_ns           = period_ns;
    rm->missed_periods      = 0;
    rm->total_periods       = 0;
    rm->next_unlock_ns      = 0;
    rm->total_awake_ns      = 0;
    rm->longest_cycle_ns    = 0;
    rm->last_unlock_time_ns = 0;
    rm->shortest_cycle_ns   = 0xFFFFFFFFFFFFFFFF;

    if (first_unlock_time_ns == RATE_MONOTONIC_IMMEDIATE_UNLOCK) {
        rm->state = RATE_MONOTONIC_START_IMMEDIATE;
    } else {
        rm->state          = RATE_MONOTONIC_START_DELAYED;
        rm->next_unlock_ns = first_unlock_time_ns;
    }
}