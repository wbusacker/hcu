/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: rate_monotonic_types.h
Purpose:  CSC data types
*/

#ifndef RATE_MONOTONIC_TYPES_H
#define RATE_MONOTONIC_TYPES_H
#include <rate_monotonic_const.h>
#include <stdint.h>

struct Rate_monotonic_t {
    uint64_t                      period_ns;           /* Desired NS between unlocks */
    uint64_t                      next_unlock_ns;      /* Uptime to next unlock */
    uint64_t                      last_unlock_time_ns; /* Uptime when the last unlock occured */
    uint64_t                      missed_periods;
    uint64_t                      total_periods;
    int64_t                       total_awake_ns;
    int64_t                       longest_cycle_ns;
    int64_t                       shortest_cycle_ns;
    enum Rate_monotonic_state_enm state;
};

#endif
