/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: spi.c
Purpose:  CSC data and initialization definitions
*/

#include <spi.h>
#include <spi_functions.h>

enum Common_status_code init_spi(void) {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    io_configure_peripheral(IO_PIN_SPI_DATA_OUT, IO_PERIPHERAL_SPI_1_DATA_OUT);
    io_configure_peripheral(IO_PIN_SPI_DATA_IN, IO_PERIPHERAL_SPI_1_DATA_IN);
    io_configure_peripheral(IO_PIN_SPI_CLOCK, IO_PERIPHERAL_SPI_1_CLOCK);
    io_configure_peripheral(IO_PIN_SPI_CLOCK, IO_PERIPHERAL_SPI_1_CLOCK_IN);

    TRISB |= (1 << 13);

    /* Turn off the SPI engine as we're setting it up */
    SPI1CON1Lbits.SPIEN   = 0;
    SPI1CON1Lbits.SPISIDL = 1;
    SPI1CON1Lbits.DISSDO  = 0;
    SPI1CON1Lbits.MODE32  = 0;
    SPI1CON1Lbits.MODE16  = 0;
    SPI1CON1Lbits.SMP     = 0;
    SPI1CON1Lbits.CKE     = 0;
    SPI1CON1Lbits.SSEN    = 0;
    SPI1CON1Lbits.CKP     = 0;
    SPI1CON1Lbits.MSTEN   = 1;
    SPI1CON1Lbits.DISSDI  = 0;
    SPI1CON1Lbits.DISSCK  = 0;
    SPI1CON1Lbits.MCLKEN  = 0;
    SPI1CON1Lbits.SPIFE   = 0;
    SPI1CON1Lbits.ENHBUF  = 1;

    SPI1CON1Hbits.AUDEN     = 0;
    SPI1CON1Hbits.SPISGNEXT = 0;
    SPI1CON1Hbits.IGNROV    = 1;
    SPI1CON1Hbits.IGNTUR    = 1;
    SPI1CON1Hbits.AUDMONO   = 0;
    SPI1CON1Hbits.URDTEN    = 0;
    SPI1CON1Hbits.AUDMOD    = 0;
    SPI1CON1Hbits.FRMEN     = 0;
    SPI1CON1Hbits.FRMSYNC   = 0;
    SPI1CON1Hbits.FRMPOL    = 0;
    SPI1CON1Hbits.MSSEN     = 0;
    SPI1CON1Hbits.FRMSYPW   = 0;
    SPI1CON1Hbits.FRMCNT    = 0;

    SPI1CON2Lbits.WLENGTH = 7;

    SPI1IMSKLbits.FRMERREN = 0;
    SPI1IMSKLbits.BUSYEN   = 0;
    SPI1IMSKLbits.SPITUREN = 0;
    SPI1IMSKLbits.SRMTEN   = 0;
    SPI1IMSKLbits.SPIROVEN = 0;
    SPI1IMSKLbits.SPIRBEN  = 0;
    SPI1IMSKLbits.SPITBEN  = 0;
    SPI1IMSKLbits.SPITBFEN = 0;
    SPI1IMSKLbits.SPIRBFEN = 0;

    SPI1IMSKHbits.RXWIEN = 0;
    SPI1IMSKHbits.RXMSK  = 0;
    SPI1IMSKHbits.TXWIEN = 0;
    SPI1IMSKHbits.TXMSK  = 0;

    spi_num_devices = 0;

    for (uint8_t i = 0; i < SPI_MAX_DEVICES; i++) {
        spi_devices[i].io_number           = 0;
        spi_devices[i].baud_rate_gen_value = 0;
        spi_devices[i].spi_mode            = SPI_MODE_0;
    }

    return return_code;
}

enum Common_status_code teardown_spi(void) {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    return return_code;
}
