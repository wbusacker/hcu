/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: spi.h
Purpose:  External CSC Header
*/

#ifndef SPI_H
#define SPI_H
#include <common.h>
#include <io.h>
#include <spi_const.h>
#include <spi_types.h>

enum Common_status_code init_spi(void);
enum Common_status_code teardown_spi(void);

enum Common_status_code cycle_spi(uint64_t cycle_count, void* arg);

SPI_device_id spi_register_device(uint8_t            chip_select_io_number,
                                  enum IO_active_enm chip_select_io_state,
                                  uint64_t           clock_speed_hz,
                                  enum SPI_modes     spi_mode);

void spi_transaction(SPI_device_id id, uint8_t* tx_buffer, uint8_t* rx_buffer, uint8_t num_bytes);

#endif
