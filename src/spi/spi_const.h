/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: spi_const.h
Purpose:  CSC constants
*/

#ifndef SPI_CONST_H
#define SPI_CONST_H

#define SPI_MAX_DEVICES (32)

#define SPI_CON1L_SETTINGS_BASE (0x2021)
#define SPI_CON1H_SETTINGS_BASE (0x3000)
#define SPI_CON2L_SETTINGS_BASE (0x0007)
#define SPI_MKL_SETTINGS_BASE   (0x0000)
#define SPI_MKH_SETTINGS_BASE   (0x0000)

enum SPI_modes {
    SPI_MODE_0, /* Clock Idle Low, Data Read on First Edge */
    SPI_MODE_1, /* Clock Idle Low, Data Read on Second Edge */
    SPI_MODE_2, /* Clock Idle High, Data Read on First Edge */
    SPI_MODE_3  /* Clock Idle High, Data Read on Second Edge */
};

#endif
