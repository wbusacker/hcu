/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: spi_data.c
Purpose:  CSC data definition
*/

#include <spi_data.h>

struct SPI_device spi_devices[SPI_MAX_DEVICES];
uint8_t           spi_num_devices;