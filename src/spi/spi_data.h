/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: spi_data.h
Purpose:  CSC data declaration
*/

#ifndef SPI_DATA_H
#define SPI_DATA_H
#include <spi_const.h>
#include <spi_types.h>

extern struct SPI_device spi_devices[SPI_MAX_DEVICES];

extern uint8_t spi_num_devices;

#endif
