/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: spi_functions.h
Purpose:  CSC local function declarations
*/

#ifndef SPI_FUNCTIONS_H
#define SPI_FUNCTIONS_H
#include <spi.h>
#include <spi_data.h>

#endif
