/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: spi_template.c
Purpose:  Example Function File
*/

#include <spi_functions.h>
#include <uart.h>

SPI_device_id spi_register_device(uint8_t            chip_select_io_number,
                                  enum IO_active_enm chip_select_io_state,
                                  uint64_t           clock_speed_hz,
                                  enum SPI_modes     spi_mode) {

    SPI_device_id new_device_id = SPI_MAX_DEVICES;

    if (spi_num_devices < SPI_MAX_DEVICES) {
        io_configure_discrete(chip_select_io_number, IO_OUT, chip_select_io_state);

        uart_printf("IO %d loaded\n", chip_select_io_number);

        spi_devices[spi_num_devices].io_number = chip_select_io_number;
        spi_devices[spi_num_devices].spi_mode  = spi_mode;

        uart_printf("SPI Mode %d\n", spi_mode);

        double desired_speed_hz_d                        = clock_speed_hz;
        double desired_baud_d                            = (COMMON_FCY / (2.0 * desired_speed_hz_d)) - 1;
        spi_devices[spi_num_devices].baud_rate_gen_value = desired_baud_d;

        uart_printf("Resulting BAUD %d\n", spi_devices[spi_num_devices].baud_rate_gen_value);

        new_device_id = spi_num_devices;

        spi_num_devices++;
    }

    return new_device_id;
}