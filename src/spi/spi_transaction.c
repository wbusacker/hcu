/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: spi_template.c
Purpose:  Example Function File
*/

#include <spi_functions.h>
#include <stddef.h>
#include <uart.h>

void spi_transaction(SPI_device_id id, uint8_t* tx_buffer, uint8_t* rx_buffer, uint8_t num_bytes) {

    if (id < spi_num_devices) {

        SPI1CON1bits.SPIEN = 0;

        /* Ensure RX Buffer is empty */
        while (SPI1STATHbits.RXELM != 0) {
            volatile uint8_t garbage = SPI1BUFLbits.DATA;
            (void)garbage;
            uart_printf("Leftover Data in RX buffer\n");
        }

        SPI1CON1L = SPI_CON1L_SETTINGS_BASE;
        SPI1CON1H = SPI_CON1H_SETTINGS_BASE;
        SPI1CON2L = SPI_CON2L_SETTINGS_BASE;
        SPI1STATL = 0;
        SPI1STATH = 0;
        SPI1BRGL  = spi_devices[id].baud_rate_gen_value;
        SPI1IMSKL = SPI_MKL_SETTINGS_BASE;
        SPI1IMSKH = SPI_MKH_SETTINGS_BASE;

        switch (spi_devices[id].spi_mode) {
            case SPI_MODE_0:
                SPI1CON1Lbits.CKE = 0;
                SPI1CON1Lbits.CKP = 0;
                break;
            case SPI_MODE_1:
                SPI1CON1Lbits.CKE = 1;
                SPI1CON1Lbits.CKP = 0;
                break;
            case SPI_MODE_2:
                SPI1CON1Lbits.CKE = 0;
                SPI1CON1Lbits.CKP = 1;
                break;
            case SPI_MODE_3:
                SPI1CON1Lbits.CKE = 1;
                SPI1CON1Lbits.CKP = 1;
                break;
        }

        SPI1CON1bits.SPIEN = 1;

        io_set(spi_devices[id].io_number, IO_SET);

        for (uint8_t i = 0; i < num_bytes; i++) {

            /* Make sure the TX buffer isn't full */
            while (SPI1STATHbits.TXELM == 32)
                ;

            uint8_t data_to_send = 0;
            if (tx_buffer != NULL) {
                data_to_send = tx_buffer[i];
            }

            SPI1BUFL = data_to_send;

            if (rx_buffer != NULL) {
                /* Wait for a single byte to come in */
                while (SPI1STATHbits.RXELM == 0)
                    ;
                uint8_t data_rx = SPI1BUFL;
                // uart_printf("RX%02X\n", data_rx);
                rx_buffer[i] = data_rx;
            }
        }

        /* Wait for the FIFO to empty */
        while (SPI1STATHbits.TXELM != 0)
            ;

        while (SPI1STATLbits.SPIBUSY == 1)
            ;

        io_set(spi_devices[id].io_number, IO_CLEAR);
    }
}