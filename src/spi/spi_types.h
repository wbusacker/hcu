/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: spi_types.h
Purpose:  CSC data types
*/

#ifndef SPI_TYPES_H
#define SPI_TYPES_H
#include <spi_const.h>
#include <stdint.h>

typedef uint8_t SPI_device_id;

struct SPI_device {
    uint8_t        io_number;
    uint16_t       baud_rate_gen_value;
    enum SPI_modes spi_mode;
};

#endif
