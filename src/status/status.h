/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: status.h
Purpose:  External CSC Header
*/

#ifndef STATUS_H
#define STATUS_H
#include <common.h>
#include <status_const.h>
#include <status_types.h>

enum Common_status_code init_status(void);
enum Common_status_code teardown_status(void);

enum Common_status_code cycle_status(uint64_t cycle_count, void* arg);

#endif
