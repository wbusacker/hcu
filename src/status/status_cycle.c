/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: status_cycle.c
Purpose:  CSC data and initialization definitions
*/

#include <io.h>
#include <status.h>
#include <status_functions.h>
#include <stdio.h>
#include <telemetry.h>
#include <uart.h>
#include <uptime.h>

enum Common_status_code cycle_status(uint64_t cycle_count, void* arg) {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    int16_t adc_count_0    = mcp3301_get_raw_value(status_adc_0);
    int16_t adc_count_1    = mcp3301_get_raw_value(status_adc_1);

    int16_t filtered_adc_0 = fir_run_fast_filter(&status_fast_filter_0, adc_count_0);
    int16_t filtered_adc_1 = fir_run_fast_filter(&status_fast_filter_1, adc_count_1);

    float voltage_division_0 = filtered_adc_0 / 4096.0;
    float resistance_0       = (1000.0 * voltage_division_0) / (1 - voltage_division_0);
    float estimated_temp_0   = (((resistance_0 - 1000) * 0.25) * (9.0 / 5.0)) + 32;

    float voltage_division_1 = filtered_adc_1 / 4096.0;
    float resistance_1       = (1000.0 * voltage_division_1) / (1 - voltage_division_1);
    float estimated_temp_1   = (((resistance_1 - 1000) * 0.25) * (9.0 / 5.0)) + 32;

    telemetry_buffer_measurand(TELEMETRY_ID_ADC_0_RAW_VALUE, &adc_count_0);
    telemetry_buffer_measurand(TELEMETRY_ID_ADC_0_FILTERED_VALUE, &filtered_adc_0);
    telemetry_buffer_measurand(TELEMETRY_ID_TMP_SENSOR_0, &estimated_temp_0);

    telemetry_buffer_measurand(TELEMETRY_ID_ADC_1_RAW_VALUE, &adc_count_1);
    telemetry_buffer_measurand(TELEMETRY_ID_ADC_1_FILTERED_VALUE, &filtered_adc_1);
    telemetry_buffer_measurand(TELEMETRY_ID_TMP_SENSOR_1, &estimated_temp_1);

    io_toggle(IO_PIN_STATUS_LED);

    return return_code;
}
