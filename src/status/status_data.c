/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: status_data.c
Purpose:  CSC data definition
*/

#include <status_data.h>

struct FIR_fast_filter status_fast_filter_0;
struct FIR_fast_filter status_fast_filter_1;

MCP3301_device_id      status_adc_0;
MCP3301_device_id      status_adc_1;