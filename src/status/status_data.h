/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: status_data.h
Purpose:  CSC data declaration
*/

#ifndef STATUS_DATA_H
#define STATUS_DATA_H
#include <fir.h>
#include <mcp3301.h>
#include <spi.h>
#include <status_const.h>
#include <status_types.h>

extern struct FIR_fast_filter status_fast_filter_0;
extern struct FIR_fast_filter status_fast_filter_1;

extern MCP3301_device_id      status_adc_0;
extern MCP3301_device_id      status_adc_1;

#endif
