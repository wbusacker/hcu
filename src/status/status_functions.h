/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: status_functions.h
Purpose:  CSC local function declarations
*/

#ifndef STATUS_FUNCTIONS_H
#define STATUS_FUNCTIONS_H
#include <status.h>
#include <status_data.h>

#endif
