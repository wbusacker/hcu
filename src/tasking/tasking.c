/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: tasking.c
Purpose:  CSC data and initialization definitions
*/

#include <tasking.h>
#include <tasking_functions.h>
#include <telemetry.h>

enum Common_status_code init_tasking(void) {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    telemetry_register_measurand(TELEMETRY_ID_CPU_UTILIZATION, TELEMETRY_DATA_TYPE_FLOAT);

    tasking_main_cycle_count = 0;

    return return_code;
}

enum Common_status_code teardown_tasking(void) {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    return return_code;
}
