/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: tasking.h
Purpose:  External CSC Header
*/

#ifndef TASKING_H
#define TASKING_H
#include <common.h>
#include <tasking_const.h>
#include <tasking_types.h>

enum Common_status_code init_tasking(void);
enum Common_status_code teardown_tasking(void);

enum Common_status_code tasking_cycle(struct Tasking_entry* tasks, uint16_t num_tasks);

#endif
