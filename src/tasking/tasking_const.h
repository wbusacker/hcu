/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: tasking_const.h
Purpose:  CSC constants
*/

#ifndef TASKING_CONST_H
#define TASKING_CONST_H

#define TASKING_BASE_FREQUENCY_HZ (50)
#define TASKING_BASE_PERIOD_NS    (1000000000 / TASKING_BASE_FREQUENCY_HZ)

/* The tasking engine makes the assumption that all frequencies are periodic with the base frequency
    and that all frequencies are hyper-periodic with the lowest frequency
*/
enum Tasking_frequency {
    TASKING_FREQUENCY_50_HZ,
    TASKING_FREQUENCY_10_HZ,
    TASKING_FREQUENCY_1_HZ,
    TASKING_FREQUENCY_1_2_HZ,
    TASKING_FREQUENCY_1_10_HZ,
    TASKING_FREQUENCY_1_30_HZ,
    TASKING_FREQUENCY_1_60_HZ,
    TASKING_FREQUENCY_1_300_HZ,
    TASKING_NUM_FREQUENCIES
};

#endif
