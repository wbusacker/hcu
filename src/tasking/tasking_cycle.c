/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: tasking_cycle.c
Purpose:  CSC data and initialization definitions
*/

#include <io.h>
#include <rate_monotonic.h>
#include <string.h>
#include <tasking.h>
#include <tasking_functions.h>
#include <telemetry.h>
#include <uart.h>
#include <uptime.h>

enum Common_status_code tasking_cycle(struct Tasking_entry* tasks, uint16_t num_tasks) {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    uart_printf("Starting Periodic Processing\n");

    struct Rate_monotonic_t rm_control;

    rate_monotonic_init(&rm_control, TASKING_BASE_PERIOD_NS, RATE_MONOTONIC_IMMEDIATE_UNLOCK);

    bool     keep_cycling         = true;
    uint16_t hyper_period_counter = 0;

    while (keep_cycling) {

        // io_set(IO_PIN_STATUS_LED, IO_CLEAR);
        rate_monotonic_cycle(&rm_control);
        // io_set(IO_PIN_STATUS_LED, IO_SET);

        for (uint8_t frequency = 0; frequency < TASKING_NUM_FREQUENCIES; frequency++) {

            if ((hyper_period_counter % tasking_frequency_to_counts[frequency]) == 0) {

                for (uint64_t task = 0; task < num_tasks; task++) {

                    if (tasks[task].frequency == frequency) {

                        uint64_t                time_start = uptime_nanoseconds();
                        enum Common_status_code status =
                          tasks[task].function(tasking_main_cycle_count, tasks[task].arguments);
                        uint64_t time_stop  = uptime_nanoseconds();
                        uint64_t delta_time = time_stop - time_start;

                        tasks[task].cycle_count++;
                        tasks[task].total_cycle_execution_time_ns += delta_time;

                        if (delta_time > tasks[task].maximum_cycle_execution_time_ns) {
                            tasks[task].maximum_cycle_execution_time_ns = delta_time;
                        }

                        if (delta_time < tasks[task].minimum_cycle_execution_time_ns) {
                            tasks[task].minimum_cycle_execution_time_ns = delta_time;
                        }

                        if (status != COMMON_STATUS_OK) {
                            keep_cycling = false;
                        }
                    }
                }
            }
        }

        int64_t  min;
        int64_t  max;
        int64_t  ave;
        uint64_t missed;

        rate_monotonic_get_statistics(&rm_control, &ave, &min, &max, &missed);

        float tasking_cycle_average_util = ((float)ave * 100.0) / (float)TASKING_BASE_PERIOD_NS;

        telemetry_buffer_measurand(TELEMETRY_ID_CPU_UTILIZATION, &tasking_cycle_average_util);

        if (hyper_period_counter == tasking_frequency_to_counts[TASKING_NUM_FREQUENCIES - 1]) {
            hyper_period_counter = -1;
        }

        tasking_main_cycle_count++;
        hyper_period_counter++;
    }

    return return_code;
}
