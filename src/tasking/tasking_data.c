/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: tasking_data.c
Purpose:  CSC data definition
*/

#include <tasking_data.h>

uint16_t tasking_frequency_to_counts[TASKING_NUM_FREQUENCIES] = {
  1,     /* TASKING_FREQUENCY_50_HZ    0.02 Seconds */
  5,     /* TASKING_FREQUENCY_10_HZ    0.1  Seconds */
  50,    /* TASKING_FREQUENCY_1_HZ     1    Second  */
  100,   /* TASKING_FREQUENCY_1_2_HZ   2    Seconds */
  500,   /* TASKING_FREQUENCY_1_10_HZ  10   Seconds */
  1500,  /* TASKING_FREQUENCY_1_30_HZ  30   Seconds */
  3000,  /* TASKING_FREQUENCY_1_60_HZ  1    Minute  */
  15000, /* TASKING_FREQUENCY_1_300_HZ 5    Minutes */
};

uint64_t tasking_main_cycle_count;