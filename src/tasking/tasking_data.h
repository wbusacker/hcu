/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: tasking_data.h
Purpose:  CSC data declaration
*/

#ifndef TASKING_DATA_H
#define TASKING_DATA_H
#include <stdint.h>
#include <tasking_const.h>
#include <tasking_types.h>

extern uint16_t tasking_frequency_to_counts[TASKING_NUM_FREQUENCIES];
extern uint64_t tasking_main_cycle_count;

#endif
