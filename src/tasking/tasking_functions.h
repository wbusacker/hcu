/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: tasking_functions.h
Purpose:  CSC local function declarations
*/

#ifndef TASKING_FUNCTIONS_H
#define TASKING_FUNCTIONS_H
#include <tasking.h>
#include <tasking_data.h>

#endif
