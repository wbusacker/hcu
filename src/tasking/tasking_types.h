/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: tasking_types.h
Purpose:  CSC data types
*/

#ifndef TASKING_TYPES_H
#define TASKING_TYPES_H
#include <tasking_const.h>

struct Tasking_entry {
    enum Common_status_code (*function)(uint64_t cycle_count, void* arg);
    enum Tasking_frequency frequency;
    void*                  arguments;
    uint64_t               cycle_count;
    uint64_t               total_cycle_execution_time_ns;
    uint64_t               minimum_cycle_execution_time_ns;
    uint64_t               maximum_cycle_execution_time_ns;
};

#endif
