/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: telemetry.c
Purpose:  CSC data and initialization definitions
*/

#include <telemetry.h>
#include <telemetry_functions.h>

enum Common_status_code init_telemetry() {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    for (uint16_t i = 0; i < TELEMETRY_ID_NUM_IDS; i++) {
        telemetry_measurands[i].buffer_index = 0;
        telemetry_measurands[i].type         = TELEMETRY_DATA_TYPE_UNKNOWN;
    }

    return return_code;
}

enum Common_status_code teardown_telemetry() {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    return return_code;
}
