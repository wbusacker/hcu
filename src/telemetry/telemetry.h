/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: telemetry.h
Purpose:  External CSC Header
*/

#ifndef TELEMETRY_H
#define TELEMETRY_H
#include <common.h>
#include <telemetry_const.h>
#include <telemetry_types.h>

enum Common_status_code init_telemetry(void);
enum Common_status_code teardown_telemetry(void);

enum Common_status_code cycle_telemetry(uint64_t cycle_count, void* arg);

void telemetry_announce_definition(void);

void telemetry_register_measurand(enum Telemetry_id id, enum Telemetry_data_types type);

void telemetry_buffer_measurand(enum Telemetry_id id, void* data);

#endif
