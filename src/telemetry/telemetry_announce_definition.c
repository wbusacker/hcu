/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: telemetry_template.c
Purpose:  Example Function File
*/

#include <telemetry_functions.h>
#include <uart.h>

void telemetry_announce_definition(void) {

    uart_printf("TELEMETRY DEFINITION\n");

    uint16_t tlm_index = 0;
    while (tlm_index < telemetry_master_buffer_len) {

        for (uint16_t i = 0; i < TELEMETRY_ID_NUM_IDS; i++) {
            if ((telemetry_measurands[i].type != TELEMETRY_DATA_TYPE_UNKNOWN) &&
                (telemetry_measurands[i].buffer_index == tlm_index)) {
                uart_printf("%d,%c,", i, telemetry_python_pack_names[telemetry_measurands[i].type]);
                tlm_index += telemetry_type_sizing[telemetry_measurands[i].type];
            }
        }
    }

    uart_printf("\n");
}