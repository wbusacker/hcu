/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: telemetry_template.c
Purpose:  Example Function File
*/

#include <string.h>
#include <telemetry_functions.h>
#include <uart.h>

void telemetry_buffer_measurand(enum Telemetry_id id, void* data) {

    if (id < TELEMETRY_ID_NUM_IDS) {

        memcpy(&(telemetry_master_buffer[telemetry_measurands[id].buffer_index]),
               data,
               telemetry_type_sizing[telemetry_measurands[id].type]);
    }
}