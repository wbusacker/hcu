/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: telemetry_cycle.c
Purpose:  CSC data and initialization definitions
*/

#include <telemetry.h>
#include <telemetry_functions.h>
#include <uart.h>

enum Common_status_code cycle_telemetry(uint64_t cycle_count, void* arg) {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    uint16_t num_bytes_to_send = telemetry_master_buffer_len * 2;

    uint8_t to_send_buffer[TELEMETRY_MASTER_BUFFER_LEN_BYTES * 2];
    uint8_t to_hex_map[16] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    for (uint16_t i = 0; i < telemetry_master_buffer_len; i++) {
        uint8_t source_data = telemetry_master_buffer[i];

        uint8_t tsi = i * 2;

        to_send_buffer[tsi]     = to_hex_map[((source_data >> 4) & 0x0F)];
        to_send_buffer[tsi + 1] = to_hex_map[((source_data)&0x0F)];
    }

    uart_send_buffer(to_send_buffer, num_bytes_to_send);
    uart_printf("\n");

    return return_code;
}
