/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: telemetry_data.c
Purpose:  CSC data definition
*/

#include <telemetry_data.h>

uint8_t telemetry_master_buffer[TELEMETRY_MASTER_BUFFER_LEN_BYTES];
uint8_t telemetry_master_buffer_len;

uint8_t telemetry_type_sizing[TELEMETRY_NUM_DATA_TYPES] = {[TELEMETRY_DATA_TYPE_UINT8]   = 1,
                                                           [TELEMETRY_DATA_TYPE_UINT16]  = 2,
                                                           [TELEMETRY_DATA_TYPE_UINT32]  = 4,
                                                           [TELEMETRY_DATA_TYPE_UINT64]  = 8,
                                                           [TELEMETRY_DATA_TYPE_INT8]    = 1,
                                                           [TELEMETRY_DATA_TYPE_INT16]   = 2,
                                                           [TELEMETRY_DATA_TYPE_INT32]   = 4,
                                                           [TELEMETRY_DATA_TYPE_INT64]   = 8,
                                                           [TELEMETRY_DATA_TYPE_FLOAT]   = 4,
                                                           [TELEMETRY_DATA_TYPE_DOUBLE]  = 8,
                                                           [TELEMETRY_DATA_TYPE_UNKNOWN] = 0};

struct Telemetry_measurand telemetry_measurands[TELEMETRY_ID_NUM_IDS];

uint8_t telemetry_python_pack_names[TELEMETRY_NUM_DATA_TYPES] = {[TELEMETRY_DATA_TYPE_UINT8]   = 'B',
                                                                 [TELEMETRY_DATA_TYPE_UINT16]  = 'H',
                                                                 [TELEMETRY_DATA_TYPE_UINT32]  = 'I',
                                                                 [TELEMETRY_DATA_TYPE_UINT64]  = 'L',
                                                                 [TELEMETRY_DATA_TYPE_INT8]    = 'b',
                                                                 [TELEMETRY_DATA_TYPE_INT16]   = 'h',
                                                                 [TELEMETRY_DATA_TYPE_INT32]   = 'i',
                                                                 [TELEMETRY_DATA_TYPE_INT64]   = 'l',
                                                                 [TELEMETRY_DATA_TYPE_FLOAT]   = 'f',
                                                                 [TELEMETRY_DATA_TYPE_DOUBLE]  = 'd',
                                                                 [TELEMETRY_DATA_TYPE_UNKNOWN] = 'x'};