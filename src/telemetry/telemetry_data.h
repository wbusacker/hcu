/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: telemetry_data.h
Purpose:  CSC data declaration
*/

#ifndef TELEMETRY_DATA_H
#define TELEMETRY_DATA_H
#include <stdint.h>
#include <telemetry_const.h>
#include <telemetry_types.h>

extern uint8_t telemetry_master_buffer[TELEMETRY_MASTER_BUFFER_LEN_BYTES];
extern uint8_t telemetry_master_buffer_len;
extern uint8_t telemetry_type_sizing[TELEMETRY_NUM_DATA_TYPES];

extern struct Telemetry_measurand telemetry_measurands[TELEMETRY_ID_NUM_IDS];

extern uint8_t telemetry_python_pack_names[TELEMETRY_NUM_DATA_TYPES];

#endif
