/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: telemetry_functions.h
Purpose:  CSC local function declarations
*/

#ifndef TELEMETRY_FUNCTIONS_H
#define TELEMETRY_FUNCTIONS_H
#include <telemetry.h>
#include <telemetry_data.h>

#endif
