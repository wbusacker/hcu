/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: telemetry_template.c
Purpose:  Example Function File
*/

#include <telemetry_functions.h>
#include <uart.h>

void telemetry_register_measurand(enum Telemetry_id id, enum Telemetry_data_types type) {

    if (id < TELEMETRY_ID_NUM_IDS) {
        telemetry_measurands[id].buffer_index = telemetry_master_buffer_len;
        telemetry_measurands[id].type         = type;

        telemetry_master_buffer_len += telemetry_type_sizing[telemetry_measurands[id].type];

        uart_printf("Tlm ID %d at location %d with len %d\n",
                    id,
                    telemetry_measurands[id].buffer_index,
                    telemetry_type_sizing[telemetry_measurands[id].type]);
    }
}