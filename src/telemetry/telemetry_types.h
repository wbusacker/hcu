/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: telemetry_types.h
Purpose:  CSC data types
*/

#ifndef TELEMETRY_TYPES_H
#define TELEMETRY_TYPES_H
#include <telemetry_const.h>

struct Telemetry_measurand {
    uint8_t                   buffer_index;
    enum Telemetry_data_types type;
};

#endif
