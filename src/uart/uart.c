/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  hcu
Filename: uart.c
Purpose:  CSC data and initialization definitions
*/

#include <io.h>
#include <platform.h>
#include <uart.h>
#include <uart_data.h>

enum Common_status_code init_uart(void) {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    uart_incoming_write_head = 0;
    uart_incoming_read_head  = 0;

    _U1RXIE = 1;
    io_configure_peripheral(IO_PIN_UART_RX, IO_PERIPHERAL_UART_1_RX);
    io_configure_peripheral(IO_PIN_UART_TX, IO_PERIPHERAL_UART_1_TX);

    /* Calculate Baud Generator rate */
    U1MODE = UART_MODE_SETTINGS;
    U1STA  = UART_CONTROL_SETTINGS;
    uart_configure_baud(UART_SYSTEM_BAUD_RATE);

    uart_out_enabled = true;

    uart_printf("\n\n");
    uart_printf("House Control Unit\n");
    uart_printf("Serial Startup\n");

    return return_code;
}

enum Common_status_code teardown_uart(void) {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    return return_code;
}