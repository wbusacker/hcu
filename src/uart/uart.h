/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  hcu
Filename: uart.h
Purpose:  External CSC Header
*/

#ifndef UART_H
#define UART_H
#include <common.h>
#include <stdint.h>
#include <uart_const.h>
#include <uart_types.h>

enum Common_status_code init_uart(void);
enum Common_status_code teardown_uart(void);

uint16_t uart_send_buffer(const uint8_t* buffer, uint16_t len);
uint16_t uart_collect_buffer(uint8_t* buffer, uint16_t len);
void     uart_configure_baud(uint64_t desired_baud);

void uart_printf(char* format, ...);

#endif
