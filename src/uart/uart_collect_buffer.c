/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  hcu
Filename: uart.c
Purpose:  CSC data and initialization definitions
*/

#include <platform.h>
#include <uart_functions.h>

uint16_t uart_collect_buffer(uint8_t* buffer, uint16_t len) {

    uint16_t rx_chars = 0;

    if (uart_out_enabled) {

        while ((uart_incoming_read_head != uart_incoming_write_head) && (rx_chars < len)) {
            buffer[rx_chars] = uart_incoming_data_buffer[uart_incoming_read_head];
            rx_chars++;
            uart_incoming_read_head++;
            uart_incoming_read_head %= UART_DATA_HOLDING_BUFFER_LEN;
        }
    }

    return rx_chars;
}