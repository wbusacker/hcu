/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  hcu
Filename: uart.c
Purpose:  CSC data and initialization definitions
*/

#include <math.h>
#include <platform.h>
#include <stdio.h>
#include <uart_functions.h>

void uart_configure_baud(uint64_t desired_baud) {

    U1MODEbits.BRGH = 1;

    /* We need to be doing */

    double fcy        = COMMON_FCY;
    double fmd        = UART_FAST_MODE_DIVIDER;
    double db         = desired_baud;
    double fast_brg_d = (fcy / (fmd * db)) - 1;
    U1BRG             = fast_brg_d;
}