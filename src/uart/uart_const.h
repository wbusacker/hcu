/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  hcu
Filename: uart_const.h
Purpose:  CSC constants
*/

#ifndef UART_CONST_H
#define UART_CONST_H

#define UART_SYSTEM_BAUD_RATE        (500000l)
#define UART_DATA_HOLDING_BUFFER_LEN (128)

#define UART_MAX_PRINTF_LEN (256)

#define UART_STANDARD_MODE_DIVIDER (16)
#define UART_FAST_MODE_DIVIDER     (4)

#define UART_MODE_SETTINGS    (0b1010100010000000)
#define UART_CONTROL_SETTINGS (0b1000010000010010)

enum UART_baud_rates {
    UART_BAUD_75      = 26666,
    UART_BAUD_110     = 18181,
    UART_BAUD_150     = 13332,
    UART_BAUD_300     = 6666,
    UART_BAUD_600     = 3332,
    UART_BAUD_1200    = 1666,
    UART_BAUD_1800    = 1110,
    UART_BAUD_2400    = 832,
    UART_BAUD_4800    = 416,
    UART_BAUD_7200    = 277,
    UART_BAUD_9600    = 207,
    UART_BAUD_14400   = 138,
    UART_BAUD_19200   = 103,
    UART_BAUD_31250   = 63,
    UART_BAUD_38400   = 51,
    UART_BAUD_5600    = 356,
    UART_BAUD_57600   = 34,
    UART_BAUD_76800   = 25,
    UART_BAUD_115200  = 16,
    UART_BAUD_128000  = 15,
    UART_BAUD_230400  = 8,
    UART_BAUD_250000  = 7,
    UART_BAUD_256000  = 7,
    UART_BAUD_500000  = 3,
    UART_BAUD_512000  = 3,
    UART_BAUD_1000000 = 1
};

#endif
