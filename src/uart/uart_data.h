/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  hcu
Filename: uart_data.h
Purpose:  CSC data declaration
*/

#ifndef UART_DATA_H
#define UART_DATA_H
#include <stdbool.h>
#include <stdint.h>
#include <uart_const.h>
#include <uart_types.h>

extern volatile uint8_t  uart_incoming_data_buffer[UART_DATA_HOLDING_BUFFER_LEN];
extern volatile uint16_t uart_incoming_write_head;
extern volatile uint16_t uart_incoming_read_head;

extern bool uart_out_enabled;

#endif
