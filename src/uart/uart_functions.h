/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  hcu
Filename: uart_functions.h
Purpose:  CSC local function declarations
*/

#ifndef UART_FUNCTIONS_H
#define UART_FUNCTIONS_H
#include <uart.h>
#include <uart_data.h>

#endif
