/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: uart_interrupt.c
Purpose:  Example Function File
*/

#include <platform.h>
#include <uart_functions.h>

void __attribute__((interrupt, auto_psv)) _U1RXInterrupt(void) {

    uart_incoming_data_buffer[uart_incoming_write_head] = U1RXREG;
    uart_incoming_write_head++;
    uart_incoming_write_head %= UART_DATA_HOLDING_BUFFER_LEN;
    _U1RXIF = 0;
}
