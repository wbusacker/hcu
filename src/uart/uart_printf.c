/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  hcu
Filename: uart.c
Purpose:  CSC data and initialization definitions
*/

#include <platform.h>
#include <stdarg.h>
#include <stdio.h>
#include <uart_functions.h>

void uart_printf(char* format, ...) {

    if (uart_out_enabled) {

        static uint8_t uart_printf_buffer[UART_MAX_PRINTF_LEN];

        va_list arg_list;
        va_start(arg_list, format);

        int num_bytes = vsnprintf((char*)uart_printf_buffer, UART_MAX_PRINTF_LEN - 1, format, arg_list);

        va_end(arg_list);

        if (num_bytes == -1) {
            num_bytes = UART_MAX_PRINTF_LEN - 1;
        }
        uart_send_buffer(uart_printf_buffer, num_bytes);
    }
}