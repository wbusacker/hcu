/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  hcu
Filename: uart.c
Purpose:  CSC data and initialization definitions
*/

#include <platform.h>
#include <uart_functions.h>

uint16_t uart_send_buffer(const uint8_t* buffer, uint16_t len) {

    uint16_t sent_chars = 0;
    if (uart_out_enabled) {

        uint16_t num_chars = len;
        if (len == 0) {
            while (buffer[num_chars] != 0) {
                num_chars++;
            }
        }

        for (sent_chars = 0; sent_chars < num_chars; sent_chars++) {
            U1TXREG = buffer[sent_chars];
            while ((U1STA & (1 << 8)) == 0) {
                __asm__ volatile("nop");
            }
        }
    }

    return sent_chars;
}