/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: uptime.c
Purpose:  CSC data and initialization definitions
*/

#include <platform.h>
#include <uptime.h>
#include <uptime_functions.h>

enum Common_status_code init_uptime(void) {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    uptime_milliseconds_counter = 0;

    /* Configure Timer 1 to go off every millisecond */
    _T1IE = 1;
    TMR1  = 0;
    // PR1   = ((COMMON_FCY * UPTIME_NS_PER_TICK) / NS_PER_S);
    PR1 = 8000;

    T1CONbits.TSIDL = 0;
    T1CONbits.TECS  = 0;
    T1CONbits.TGATE = 0;
    T1CONbits.TCKPS = 0;
    T1CONbits.TSYNC = 0;
    T1CONbits.TCS   = 0;
    T1CONbits.TON   = 1;

    return return_code;
}

enum Common_status_code teardown_uptime(void) {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    return return_code;
}
