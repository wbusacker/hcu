/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: uptime.h
Purpose:  External CSC Header
*/

#ifndef UPTIME_H
#define UPTIME_H
#include <common.h>
#include <uptime_const.h>
#include <uptime_types.h>

enum Common_status_code init_uptime(void);
enum Common_status_code teardown_uptime(void);

uint64_t uptime_nanoseconds(void);
uint64_t uptime_milliseconds(void);

#endif
