/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: uptime_data.c
Purpose:  CSC data definition
*/

#include <uptime_data.h>

volatile uint64_t uptime_milliseconds_counter = 0;