/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: uptime_data.h
Purpose:  CSC data declaration
*/

#ifndef UPTIME_DATA_H
#define UPTIME_DATA_H
#include <stdint.h>
#include <uptime_const.h>
#include <uptime_types.h>

extern volatile uint64_t uptime_milliseconds_counter;

#endif
