/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: uptime_functions.h
Purpose:  CSC local function declarations
*/

#ifndef UPTIME_FUNCTIONS_H
#define UPTIME_FUNCTIONS_H
#include <uptime.h>
#include <uptime_data.h>

#endif
