/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: uptime_template.c
Purpose:  Example Function File
*/

#include <platform.h>
#include <uptime_functions.h>

void __attribute__((interrupt, auto_psv)) _T1Interrupt(void) {
    uptime_milliseconds_counter++;
    _T1IF = 0;
}