/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: uptime_template.c
Purpose:  Example Function File
*/

#include <uptime_functions.h>

uint64_t uptime_milliseconds(void) {
    return uptime_milliseconds_counter;
}
