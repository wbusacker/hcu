/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: uptime_template.c
Purpose:  Example Function File
*/

#include <platform.h>
#include <uptime_functions.h>

uint64_t uptime_nanoseconds(void) {

    /* We need to protect for the timer rolling over while we're calculating total uptime so if we're too close wait for
     * the value go up */
    while (TMR1 > UPTIME_TIMER_THRESHOLD)
        ;
    uint64_t current_timer = TMR1;

    // return ((uptime_milliseconds_counter + 1) * NS_PER_MS) - (current_timer * (125));

    return (uptime_milliseconds_counter * 1000000) + (current_timer * 125);
}