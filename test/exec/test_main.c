#include <p24FJ64GA002.h>
#include <stdint.h>

// PIC16F1454 Configuration Bit Settings

// 'C' source line config statements

// CONFIG2
#pragma config POSCMOD  = HS     // Primary Oscillator Select (HS Oscillator mode selected)
#pragma config I2C1SEL  = PRI    // I2C1 Pin Location Select (Use default SCL1/SDA1 pins)
#pragma config IOL1WAY  = ON     // IOLOCK Protection (Once IOLOCK is set, cannot be changed)
#pragma config OSCIOFNC = OFF    // Primary Oscillator Output Function (OSC2/CLKO/RC15 functions as CLKO (FOSC/2))
#pragma config FCKSM =                                                                                                 \
  CSDCMD    // Clock Switching and Monitor (Clock switching and Fail-Safe Clock Monitor are disabled)
#pragma config FNOSC   = PRI     // Oscillator Select (Primary Oscillator (XT, HS, EC))
#pragma config SOSCSEL = SOSC    // Sec Oscillator Select (Default Secondary Oscillator (SOSC))
#pragma config WUTSEL  = LEG     // Wake-up timer Select (Legacy Wake-up Timer)
#pragma config IESO    = ON      // Internal External Switch Over Mode (IESO mode (Two-Speed Start-up) enabled)

// CONFIG1
#pragma config WDTPS  = PS32768    // Watchdog Timer Postscaler (1:32,768)
#pragma config FWPSA  = PR128      // WDT Prescaler (Prescaler ratio of 1:128)
#pragma config WINDIS = OFF        // Watchdog Timer Window (Windowed Watchdog Timer enabled; FWDTEN must be 1)
#pragma config FWDTEN = OFF        // Watchdog Timer Enable (Watchdog Timer is disabled)
#pragma config ICS    = PGx1       // Comm Channel Select (Emulator EMUC1/EMUD1 pins are shared with PGC1/PGD1)
#pragma config GWRP   = ON         // General Code Segment Write Protect (Writes to program memory are disabled)
#pragma config GCP    = OFF        // General Code Segment Code Protect (Code protection is disabled)
#pragma config JTAGEN = ON         // JTAG Port Enable (JTAG port is enabled)

int main(int argc, char** argv) {

    return (0);
}