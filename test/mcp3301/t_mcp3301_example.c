/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: mcp3301_example.c
Purpose:  CSC test module
*/

#include <mcp3301_functions.h>
#include <test_engine.h>
