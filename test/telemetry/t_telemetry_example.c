/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: telemetry_example.c
Purpose:  CSC test module
*/

#include <telemetry_functions.h>
#include <test_engine.h>
