/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  hcu
Filename: uptime_example.c
Purpose:  CSC test module
*/

#include <test_engine.h>
#include <uptime_functions.h>
