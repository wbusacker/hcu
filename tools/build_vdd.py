#!/usr/bin/python3
import hashlib
import os

def generate_file_hash(file_path):

    hash = "File Not Found"

    try:
        with open(file_path, "rb") as fh:
            h = hashlib.md5()
            h.update(fh.read())
            hash = h.hexdigest()
    except:
        pass
    
    return hash

class Compilation_unit:
    def __init__(self, target, csc, unit_name):
        self.base_name = unit_name.split(".")[0]
        self.source_path       = target + csc + "/" + unit_name

        object_append = ".o"
        if "test" in target:
            object_append = ".t.o"

        self.prod_object_path  = "bld/production_obj/" + self.base_name + object_append
        self.debug_object_path = "bld/debug_obj/" + self.base_name + object_append
        self.source_hash            = generate_file_hash(self.source_path)
        self.production_object_hash = generate_file_hash(self.prod_object_path)
        self.debug_object_hash      = generate_file_hash(self.debug_object_path)

class Computer_software_componen:

    Directory_type = {
        "src/"  : 0,
        "test/" : 1
    }

    def __init__(self, csc):

        self.csc = csc

        self.compilations_units = []

        for directory in Computer_software_componen.Directory_type:

            units = []
            working_dir = directory + csc

            if os.path.isdir(working_dir):
                for file in os.listdir(working_dir):
                    possible_file = os.path.join(working_dir, file)

                    if os.path.isfile(possible_file):
                        units.append(Compilation_unit(directory, csc, file))

            self.compilations_units.append(units)

    def __str__(self):

        entry_format = "{:5} {:30}  {:32}  {:32}  {:32}\n"

        message = "="*138 + "\n"
        message += "\nComputer Software Component: {:s}\n\n".format(self.csc)
        message += "="*138 + "\n"

        message += entry_format.format("Type", "Unit Name", "Source MD5", "Production MD5", "Debug MD5")
        for directory in Computer_software_componen.Directory_type:
            # Do all of the headers first
            for unit in self.compilations_units[Computer_software_componen.Directory_type[directory]]:
                if ".h" == unit.source_path[-2:]:
                    message += entry_format.format(directory[:-1], unit.base_name, unit.source_hash, "", "")

            # Next, everything but
            for unit in self.compilations_units[Computer_software_componen.Directory_type[directory]]:
                if ".h" != unit.source_path[-2:]:
                    message += entry_format.format(directory[:-1], unit.base_name, unit.source_hash, unit.production_object_hash, unit.debug_object_hash)

        return message

cscs = []
for entry in os.listdir("./src/"):
    possible_dir = os.path.join("./src/", entry)
    if os.path.isdir(possible_dir):
        cscs.append(Computer_software_componen(entry))

for csc in cscs:
    print(csc)